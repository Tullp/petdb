
from petdb import PetArray
from .engine import testset, arraytest

@testset
def base():

	@arraytest(expect=[1, 2, 3])
	def array_to_list(array: PetArray):
		return list(array)

	@arraytest(expect=[1, 2, 3])
	def array_iteration1(array: PetArray):
		return [item for item in array]

	@arraytest(init=[1, 2, 3], expect=True)
	def array_iteration2(array: PetArray):
		return 1 in array

	@arraytest(init=[1, 2, 3], expect=False)
	def array_iteration3(array: PetArray):
		return 4 in array

	@arraytest(expect=[1, 2, 3])
	def array_foreach(array: PetArray):
		res = []
		array.foreach(lambda item: res.append(item))
		return res

	@arraytest(init=[1, 2, 3], expect=3)
	def array_size1(array: PetArray):
		return array.size()

	@arraytest(init=[1, 2, 3], expect=2)
	def array_size2(array: PetArray):
		return array.filter({"$lt": 3}).size()

	@arraytest(init=[1, 2, 3], expect=2)
	def array_size3(array: PetArray):
		return array.size({"$lt": 3})

	@arraytest(init=[1, 2, 3, 4, 5, 6], expect=1)
	def array_size4(array: PetArray):
		return array.filter({"$gt": 1}).size({"$lt": 3})

@testset
def insertion():

	@arraytest(expect=[1, 2, 3, 4, 5], init_expectation=False)
	def insert1(array: PetArray):
		for i in range(1, 6):
			array.insert(i)
		return array.list()

	@arraytest(expect=[1, 2, 3, 4, 5], init_expectation=False)
	def insert_many1(array: PetArray):
		array.insert_many([i for i in range(1, 6)])
		return array.list()

@testset
def selection():

	@arraytest(init=[1, 2, 3, 4, 5], expect=[1, 2, 3])
	def findall1(array: PetArray):
		return array.findall({"$lt": 4})

	@arraytest(init=[1, 2, 3, 4, 5], expect=[2])
	def findall2(array: PetArray):
		return array.findall({"$eq": 2})

	@arraytest(init=[1, 2, 3, 4, 5], expect=[1, 3, 4, 5])
	def findall3(array: PetArray):
		return array.findall({"$ne": 2})

	@arraytest(init=[1, 2, 3, 4, 5], expect=[])
	def findall4(array: PetArray):
		return array.findall({"$gt": 5})

	@arraytest(init=[1, 2, 3, 4, 5], expect=TypeError("'>' not supported between instances of 'NonExistent' and 'int'"))
	def findall5(array: PetArray):
		return array.findall({"a": {"$gt": 5}})

	@arraytest(init=[1, 2, 3, 4, 5], expect=False)
	def contains1(array: PetArray):
		return array.contains({"a": {"$gt": 5}})

	@arraytest(init=[1, 2, 3, 4, 5], expect=True)
	def contains2(array: PetArray):
		return array.contains(1)

	@arraytest(init=[1, 2, 3, 4, 5], expect=False)
	def contains3(array: PetArray):
		return array.contains("1")

	@arraytest(init=[2, 3, 4, 5], expect=False)
	def contains4(array: PetArray):
		return array.contains(1)

	@arraytest(init=[2, 3, 4, 5], expect=True)
	def contains5(array: PetArray):
		return array.contains({"$or": [{"$lt": 3}, {"$gt": 5}]})

	@arraytest(init=[2, 3, 4, 5], expect=False)
	def contains6(array: PetArray):
		return array.contains({"$or": [{"$lt": 2}, {"$gt": 5}]})

	@arraytest(init=[2, 3, 4, 5], expect=False)
	def contains7(array: PetArray):
		return array.contains({"$and": [{"$lt": 3}, {"$gt": 5}]})

	@arraytest(init=[1, 2, 3, 4, 5], expect=2)
	def find1(array: PetArray):
		return array.find({"$gt": 1})

	@arraytest(init=[1, 2, 3, 4, 5], expect=None)
	def find2(array: PetArray):
		return array.find({"$gt": 5})

	@arraytest(init=[1, 2, 3, 4, 5], expect=[1, 2, 3])
	def filter1(array: PetArray):
		return array.filter({"$lt": 4}).list()

	@arraytest(init=[1, 2, 3, 4, 5], expect=[2])
	def filter2(array: PetArray):
		return array.filter({"$eq": 2}).list()

	@arraytest(init=[1, 2, 3, 4, 5], expect=[1, 3, 4, 5])
	def filter3(array: PetArray):
		return array.filter({"$ne": 2}).list()

	@arraytest(init=[1, 2, 3, 4, 5], expect=[])
	def filter4(array: PetArray):
		return array.filter({"$gt": 5}).list()

	@arraytest(init=[1, 2, 3, 4, 5], expect=[1, 2, 3, 4, 5])
	def filter5(array: PetArray):
		array.filter({"$gt": 2})
		return array.list()

@testset
def sorting():

	@arraytest(init=[3, 1, 2, 5, 4], expect=[1, 2, 3, 4, 5])
	def sort1(array: PetArray):
		return array.sort().list()

	@arraytest(init=[3, 1, 2, 5, 4], expect=[5, 4, 3, 2, 1])
	def sort2(array: PetArray):
		return array.sort(reverse=True).list()

	@arraytest(init=[3, 1, 2, 5, 4], expect=[5, 4, 3, 2, 1])
	def sort3(array: PetArray):
		return array.sort(lambda x: -x).list()

	@arraytest(init=[1, 3, 2, 5, 4], expect=[1, 3, 2, 5, 4])
	def sort4(array: PetArray):
		return array.sort("a").list()

	@arraytest(init=[1, 3, 2, 5, 4], expect=[1, 3, 2, 5, 4])
	def sort5(array: PetArray):
		return array.sort("a.0.b.c").list()

	@arraytest(init=["1", "3", "2", "5", "4"], expect=["1", "3", "2", "5", "4"])
	def sort6(array: PetArray):
		return array.sort(1).list()

	@arraytest(init=["1", "3", "2", "5", "4"], expect=["1", "3", "2", "5", "4"])
	def sort7(array: PetArray):
		return array.sort(0).list()

	@arraytest(init=["1", "3", "2", "5", "4"], expect=["1", "3", "2", "5", "4"])
	def sort8(array: PetArray):
		return array.sort("0").list()

	@arraytest(init=["1", "3", "2", "5", "4"], expect=["1", "3", "2", "5", "4"])
	def sort9(array: PetArray):
		return array.sort("1").list()

@testset
def mapping():

	@arraytest(init=[1, 2, 3, 4, 5], expect=[1, 4, 9, 16, 25])
	def map1(array: PetArray):
		return array.map(lambda x: x ** 2).list()

	@arraytest(init=[], expect=[])
	def map2(array: PetArray):
		return array.map(lambda x: x ** 2).list()

@testset
def pick():

	@arraytest(init=[[1], [2], [3], [4], [5]], expect=[1, 4, 9, 16, 25])
	def pick1(array: PetArray):
		return array.pick(0).map(lambda x: x ** 2).list()

	@arraytest(init=[[1], [2], [3], [4], [5]], expect=[1, 4, 9, 16, 25])
	def pick2(array: PetArray):
		return array.pick("0").map(lambda x: x ** 2).list()

@testset
def grouping_by():

	@arraytest(init=[{"a": 1, "b": 10}, {"a": 2, "b": 20}, {"a": 3, "b": 30}, {"a": 2, "b": 40}, {"a": 1, "b": 50}],
		expect=[{"key": 1, "items": [{"a": 1, "b": 10}, {"a": 1, "b": 50}]},
		{"key": 2, "items": [{"a": 2, "b": 20}, {"a": 2, "b": 40}]},
		{"key": 3, "items": [{"a": 3, "b": 30}]}])
	def groupby1(array: PetArray):
		return array.groupby("a").list()

	@arraytest(init=[1, 1, 2, 3, 2, 2, 1, 3],
		expect=[{"key": 1, "items": [1, 1, 1]}, {"key": 2, "items": [2, 2, 2]}, {"key": 3, "items": [3, 3]}])
	def groupby2(array: PetArray):
		return array.groupby().list()

	@arraytest(init=[1, 1, 2, 3, 2, 2, 1, 3],
		expect=[{"key": 1, "items": 3}, {"key": 2, "items": 3}, {"key": 3, "items": 2}])
	def groupby3(array: PetArray):
		return array.groupby().update({"$map": {"items": lambda items: len(items)}}).list()

	@arraytest(init=[1, 1, 2, 3, 2, 2, 1, 3],
		expect=[{"key": 1, "items": 3}, {"key": 2, "items": 3}, {"key": 3, "items": 2}])
	def groupby4(array: PetArray):
		return array.groupby().map(lambda doc: {**doc, "items": len(doc["items"])}).list()

	@arraytest(init=[[1, 10], [2, 20], [3, 30], [2, 40], [1, 50]],
		expect=[{"key": 1, "items": [[1, 10], [1, 50]]},
		{"key": 2, "items": [[2, 20], [2, 40]]},
		{"key": 3, "items": [[3, 30]]}])
	def groupby5(array: PetArray):
		return array.groupby(0).list()

	@arraytest(init=[[1, 10], [2, 20], [3, 30], [2, 40], [1, 50]],
		expect=[{"key": 1, "items": [[1, 10], [1, 50]]},
			{"key": 2, "items": [[2, 20], [2, 40]]},
			{"key": 3, "items": [[3, 30]]}])
	def groupby6(array: PetArray):
		return array.groupby("0").list()

@testset
def reducing():

	@arraytest(init=[[1, 10], [2, 20], [3, 30]], expect=14)
	def reduce1(array: PetArray):
		return array.reduce(lambda acc, item: acc + item[0] ** 2, 0)

	@arraytest(init=[1, 2, 3], expect=14)
	def reduce2(array: PetArray):
		return array.reduce(lambda acc, item: acc + item ** 2)

	@arraytest(init=[2, 3, 4], expect=29)
	def reduce3(array: PetArray):
		return array.reduce(lambda acc, item: acc + item ** 2, 0)

	@arraytest(init=[2, 3, 4], expect=27)
	def reduce4(array: PetArray):
		return array.reduce(lambda acc, item: acc + item ** 2)

@testset
def joining():

	@arraytest(init=["1", "2", "3", "4"], expect="1234")
	def join1(array: PetArray):
		return array.join("")

	@arraytest(init=["1", "2", "3", "4"], expect="1|2|3|4")
	def join2(array: PetArray):
		return array.join("|")

	@arraytest(init=[1, 2, 3, 4], expect="1 4 9 16")
	def join3(array: PetArray):
		return array.join(" ", lambda item: str(item ** 2))

@testset
def unique():

	@arraytest(init=[1, 2, 1, 1, 3, 2, 3], expect=[1, 2, 3])
	def unique1(array: PetArray):
		return array.unique().list()

	@arraytest(init=[1, 2, 1, 1, 3, 2, 3], expect=[1])
	def unique2(array: PetArray):
		return array.unique(lambda x: x > 0).list()

	@arraytest(init=[{"a": 1}, {"a": 2}, {"a": 1}, {"b": 1}, {"a": 2}, {"b": 3}], expect=[{"a": 1}, {"a": 2}, {"b": 1}])
	def unique3(array: PetArray):
		return array.unique(lambda x: x.get("a", None)).list()

	@arraytest(init=[{"a": 1}, {"a": 2}, {"a": 1}, {"b": 1}, {"a": 2}, {"b": 3}], expect=[{"a": 1}, {"a": 2}, {"b": 1}])
	def unique4(array: PetArray):
		return array.unique("a").list()
