
from petdb import PetCollection
from .engine import testset, test, read_collection_file

@testset
def base():
	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1}, {"a": 2}])
	def collection_to_list(col: PetCollection):
		return list(col.filter({"a": {"$lt": 3}}))

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1}, {"a": 2}])
	def iteration1(col: PetCollection):
		result = []
		for doc in col.filter({"a": {"$lt": 3}}):
			result.append(doc)
		return result

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[1, 2])
	def iteration2(col: PetCollection):
		result = []
		for doc in col.filter({"a": {"$lt": 3}}):
			result.append(doc["a"])
		return result

@testset
def deletion():
	@test(init=[{"a": 5, "c": 10}, {"a": 6, "c": 1}, {"a": 7, "c": 10}, {"a": 8, "c": 1}],
		expect=[{"a": 6, "c": 1}, {"a": 7, "c": 10}])
	def mutable_remove1(col: PetCollection):
		return col.filter({"a": {"$lt": 8}}).remove({"a": {"$gt": 5}})

	@test(init=[{"a": 5, "c": 10}, {"a": 6, "c": 1}, {"a": 7, "c": 10}, {"a": 8, "c": 1}],
		expect=[{"a": 5, "c": 10}, {"a": 8, "c": 1}])
	def mutable_remove2(col: PetCollection):
		col.filter({"a": {"$lt": 8}}).remove({"a": {"$gt": 5}})
		return col.list()

	@test(init=[{"a": 5, "c": 10}, {"a": 6, "c": 1}, {"a": 7, "c": 10}, {"a": 8, "c": 1}],
		expect=[{"a": 5, "c": 10}, {"a": 8, "c": 1}])
	def mutable_remove_dump(col: PetCollection):
		col.filter({"a": {"$lt": 8}}).remove({"a": {"$gt": 5}})
		return read_collection_file()

	@test(init=[{"a": 5, "c": 10}, {"a": 6, "c": 1}, {"a": 7, "c": 10}, {"a": 8, "c": 1}],
		expect=[{"a": 5, "c": 10}])
	def mutable_remove3(col: PetCollection):
		mutable = col.filter({"a": {"$lt": 8}})
		mutable.remove({"a": {"$gt": 5}})
		return mutable.list()

	@test(init=[{"a": 5, "c": 10}, {"a": 6, "c": 1}, {"a": 7, "c": 10}, {"a": 8, "c": 1}],
		expect=[{"a": 5, "c": 10}, {"a": 6, "c": 1}])
	def mutable_clear1(col: PetCollection):
		return col.filter({"a": {"$lt": 7}}).clear()

	@test(init=[{"a": 5, "c": 10}, {"a": 6, "c": 1}, {"a": 7, "c": 10}, {"a": 8, "c": 1}],
		expect=[{"a": 7, "c": 10}, {"a": 8, "c": 1}])
	def mutable_clear2(col: PetCollection):
		col.filter({"a": {"$lt": 7}}).clear()
		return col.list()

	@test(init=[{"a": 5, "c": 10}, {"a": 6, "c": 1}, {"a": 7, "c": 10}, {"a": 8, "c": 1}],
		expect=[{"a": 7, "c": 10}, {"a": 8, "c": 1}])
	def mutable_clear_dump(col: PetCollection):
		col.filter({"a": {"$lt": 7}}).clear()
		return read_collection_file()

	@test(init=[{"a": 5, "c": 10}, {"a": 6, "c": 1}, {"a": 7, "c": 10}, {"a": 8, "c": 1}], expect=[])
	def mutable_clear3(col: PetCollection):
		mutable = col.filter({"a": {"$lt": 7}})
		mutable.clear()
		return mutable.list()

@testset
def updating():

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}],
		expect=[{"a": 1}, {"a": 2, "b": 1}, {"a": 3, "b": 1}, {"a": 4}])
	def mutable_update1(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).update({"$set": {"b": 1}}, {"a": {"$lt": 4}})
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}],
		expect=[{"a": 1}, {"a": 2, "b": 1}, {"a": 3, "b": 1}, {"a": 4}])
	def mutable_update_dump1(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).update({"$set": {"b": 1}}, {"a": {"$lt": 4}})
		return read_collection_file()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}],
		expect=[{"a": 1}, {"a": 2, "b": 1}, {"a": 3, "b": 1}, {"a": 4, "b": 1}])
	def mutable_update2(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).update({"$set": {"b": 1}})
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}],
		expect=[{"a": 1}, {"a": 2, "b": 1}, {"a": 3, "b": 1}, {"a": 4, "b": 1}])
	def mutable_update_dump2(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).update({"$set": {"b": 1}})
		return read_collection_file()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=[{"a": 2, "b": 1}, {"a": 3, "b": 1}, {"a": 4}])
	def mutable_update3(col: PetCollection):
		mutable = col.filter({"a": {"$gt": 1}})
		mutable.update({"$set": {"b": 1}}, {"a": {"$lt": 4}})
		return mutable.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=[{"a": 2, "b": 1}, {"a": 3, "b": 1}, {"a": 4, "b": 1}])
	def mutable_update4(col: PetCollection):
		mutable = col.filter({"a": {"$gt": 1}})
		mutable.update({"$set": {"b": 1}})
		return mutable.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}],
		expect=[{"a": 1}, {"a": 2}, {"a": 3, "b": 1}, {"a": 4}])
	def mutable_update_one1(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).update_one({"$set": {"b": 1}}, {"a": {"$gt": 2}})
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}],
		expect=[{"a": 1}, {"a": 2}, {"a": 3, "b": 1}, {"a": 4}])
	def mutable_update_one_dump1(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).update_one({"$set": {"b": 1}}, {"a": {"$gt": 2}})
		return read_collection_file()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}],
		expect=[{"a": 1}, {"a": 2, "b": 1}, {"a": 3}, {"a": 4}])
	def mutable_update_one2(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).update_one({"$set": {"b": 1}})
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}],
		expect=[{"a": 1}, {"a": 2, "b": 1}, {"a": 3}, {"a": 4}])
	def mutable_update_one_dump2(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).update_one({"$set": {"b": 1}})
		return read_collection_file()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=[{"a": 2}, {"a": 3, "b": 1}, {"a": 4}])
	def mutable_update_one3(col: PetCollection):
		mutable = col.filter({"a": {"$gt": 1}})
		mutable.update_one({"$set": {"b": 1}}, {"a": {"$gt": 2}})
		return mutable.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=[{"a": 2, "b": 1}, {"a": 3}, {"a": 4}])
	def mutable_update_one4(col: PetCollection):
		mutable = col.filter({"a": {"$gt": 1}})
		mutable.update_one({"$set": {"b": 1}})
		return mutable.list()

@testset
def insertion():
	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def mutable_insert1(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).insert({"a": 3})
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def mutable_insert_dump1(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).insert({"a": 3})
		return read_collection_file()

	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 2}, {"a": 3}])
	def mutable_insert2(col: PetCollection):
		mutable = col.filter({"a": {"$gt": 1}})
		mutable.insert({"a": 3})
		return mutable.list()

	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}])
	def mutable_insert3(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).insert({"a": 3})
		col.filter({"a": {"$lt": 1}}).insert({"a": 4})
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}])
	def mutable_insert_dump2(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).insert({"a": 3})
		col.filter({"a": {"$lt": 1}}).insert({"a": 4})
		return read_collection_file()

	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 2}, {"a": 3}, {"a": 4}])
	def mutable_insert4(col: PetCollection):
		mutable = col.filter({"a": {"$gt": 1}})
		mutable.insert({"a": 3})
		mutable.insert({"a": 4})
		return mutable.list()

	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def mutable_insert_many1(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).insert_many([{"a": 3}])
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def mutable_insert_many_dump1(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).insert_many([{"a": 3}])
		return read_collection_file()

	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 2}, {"a": 3}])
	def mutable_insert_many2(col: PetCollection):
		mutable = col.filter({"a": {"$gt": 1}})
		mutable.insert_many([{"a": 3}])
		return mutable.list()

	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}])
	def mutable_insert_many3(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).insert_many([{"a": 3}, {"a": 4}])
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}])
	def mutable_insert_many_dump2(col: PetCollection):
		col.filter({"a": {"$gt": 1}}).insert_many([{"a": 3}, {"a": 4}])
		return read_collection_file()

	@test(init=[{"a": 1}, {"a": 2}], expect=[{"a": 2}, {"a": 3}, {"a": 4}])
	def mutable_insert_many4(col: PetCollection):
		mutable = col.filter({"a": {"$gt": 1}})
		mutable.insert_many([{"a": 3}, {"a": 4}])
		return mutable.list()

@testset
def selection():
	@test(init=[{"a": 2}, {"a": 3}], expect={"a": 1})
	def mutable_get_by_id1(col: PetCollection):
		doc_id = col.insert({"a": 1})["_id"]
		return col.filter({"a": {"$lt": 3}}).get(doc_id)

	@test(expect=None)
	def mutable_get_by_id2(col: PetCollection):
		doc_id = col.insert({"a": 1})["_id"]
		return col.filter({"a": {"$gt": 1}}).get(doc_id)

	@test(expect={"a": 5, "b": 10})
	def mutable_find1(col: PetCollection):
		return col.filter({}).find({"a": 5})

	@test(init=[{"a": 5, "b": 10}], expect=None)
	def mutable_find2(col: PetCollection):
		return col.filter({}).find({"a": 10})

	@test(expect={"a": 5, "b": 10, "c": 20})
	def mutable_find3(col: PetCollection):
		return col.filter({}).find({"a": 5, "c": 20})

	@test(expect={"a": 5, "b": 10, "c": {"d": 20}})
	def mutable_find4(col: PetCollection):
		return col.filter({}).find({"a": 5, "c.d": 20})

	@test(expect={"a": 5, "b": 10, "c": {"d": 20}})
	def mutable_find5(col: PetCollection):
		return col.filter({}).find({"a": 5, "c": {"d": 20}})

	@test(expect={"a": 5, "b": 10, "c": {"d": {"e": 20}}})
	def mutable_find6(col: PetCollection):
		return col.filter({}).find({"a": 5, "c": {"d.e": 20}})

	@test(expect={"a": 5, "b": 10, "c": {"d": {"e": 20}}})
	def mutable_find7(col: PetCollection):
		return col.filter({}).find({"a": 5, "c.d": {"e": 20}})

	@test(expect={"a": 5, "b": 10, "c": {"d": {"e": 20}}})
	def mutable_find8(col: PetCollection):
		return col.filter({}).find({"a": 5, "c": {"d": {"e": 20}}})

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect={"a": 1})
	def mutable_find9(col: PetCollection):
		return col.filter({"a": {"$lt": 4}}).find({"a": {"$gte": 1}})

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=None)
	def mutable_find10(col: PetCollection):
		return col.filter({"a": {"$lt": 4}}).find({"a": {"$gte": 4}})
