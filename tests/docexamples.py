
from .engine import arraytest
from petdb import PetArray, NON_EXISTENT, NonExistent

@arraytest()
def docs_query_operators_examples(col: PetArray):

	doc = col.insert({"item": {"amount": 10}})

	assert col.find({"item.amount": 10}) == doc
	assert col.find({"item.amount": {"$eq": 10}}) == doc
	assert col.find({"item.amount": {"$eq": 0}}) is None

	assert col.find({"item.amount": {"$ne": 0}}) == doc
	assert col.find({"item.amount": {"$ne": 10}}) is None

	assert col.find({"item.amount": {"$gt": 9}}) == doc
	assert col.find({"item.amount": {"$gt": 10}}) is None

	assert col.find({"item.amount": {"$gte": 10}}) == doc
	assert col.find({"item.amount": {"$gte": 11}}) is None

	assert col.find({"item.amount": {"$lt": 11}}) == doc
	assert col.find({"item.amount": {"$lt": 10}}) is None

	assert col.find({"item.amount": {"$lte": 10}}) == doc
	assert col.find({"item.amount": {"$lt": 9}}) is None

	assert col.find({"item.amount": {"$in": [5, 10, 15]}}) == doc
	assert col.find({"item.amount": {"$in": [5, 15]}}) is None

	assert col.find({"item.amount": {"$nin": [5, 15]}}) == doc
	assert col.find({"item.amount": {"$nin": [5, 10, 15]}}) is None

	assert col.find({"$and": [{"item.amount": {"$gt": 5}}, {"item.amount": {"$lt": 15}}]}) == doc
	assert col.find({"item.amount": {"$and": [{"$gt": 5}, {"$lt": 15}]}}) == doc
	assert col.find({"item.amount": {"$and": [{"$gt": 5}, {"$lt": 10}]}}) is None

	assert col.find({"$or": [{"item.amount": {"$gt": 9}}, {"item.amount": {"$lt": 5}}]}) == doc
	assert col.find({"item.amount": {"$or": [{"$gt": 9}, {"$lt": 5}]}}) == doc
	assert col.find({"item.amount": {"$or": [{"$gt": 10}, {"$lt": 5}]}}) is None

	assert col.find({"$not": {"item.amount": 5}}) == doc
	assert col.find({"$not": {"item": {"amount": 5}}}) == doc
	assert col.find({"$not": {"item.amount": {"$gt": 10}}}) == doc
	assert col.find({"item.amount": {"$not": {"gt": 10}}}) == doc
	assert col.find({"item": {"amount": {"$not": {"gt": 10}}}}) == doc
	assert col.find({"$not": {"item.amount": 10}}) is None

	assert col.find({"item.amount": {"$exists": True}}) == doc
	assert col.find({"item.amount": {"$exists": False}}) is None
	assert col.find({"item.name": {"$exists": False}}) == doc
	try: col.find({"item.price": {"$lte": 250}})
	except TypeError as e: assert e.args[0] == "'<=' not supported between instances of 'NonExistent' and 'int'"
	assert col.find({"item.name": NON_EXISTENT}) == doc
	assert col.find({"item.price": {"$exists": True, "$lte": 250}}) is None

	assert col.find({"item.amount": {"$is": int}}) == doc
	assert col.find({"item.amount": {"$is": str}}) is None
	assert col.find({"item.name": {"$is": NonExistent}}) == doc

	assert col.find({"$where": lambda doc: "item" in doc and "amount" in doc["item"]}) == doc
	assert col.find({"item": {"amount": {"$where": lambda x: x % 2 == 0}}}) == doc
	assert col.find({"item": {"$where": lambda subdoc: subdoc["amount"] > 5}}) == doc
	assert col.find({"item": {"$where": lambda subdoc: subdoc["amount"] > 15}}) is None

@arraytest()
def docs_query_operators_examples_2(col: PetArray):

	col.insert_many([
		{"sku": "abc123"},
		{"sku": "abc789"},
		{"sku": "xyz456"},
		{"sku": "xyz789"},
		{"sku": "Abc789"},
	])

	assert col.filter({"sku": {"$regex": "789$"}}).pick("sku").list() == ["abc789", "xyz789", "Abc789"]
	assert col.filter({"sku": {"$regex": "^xyz"}}).pick("sku").list() == ["xyz456", "xyz789"]
	assert col.filter({"sku": {"$regex": "[aA]bc"}}).pick("sku").list() == ["abc123", "abc789", "Abc789"]

@arraytest()
def docs_array_query_operators_examples(col: PetArray):

	doc = col.insert({"items": [82, 85, 88]})

	assert col.find({"items": {"$size": 3}}) == doc
	assert col.find({"items": {"$size": {"$lt": 10}}}) == doc
	assert col.find({"items": {"$size": {"$or": [{"$gt": 10}, {"$lte": 3}]}}}) == doc

	assert col.find({"items": {"$elemMatch": {"$gte": 80, "$lt": 85}}}) == doc
	assert col.find({"items": {"$elemMatch": {"$gte": 83, "$lt": 85}}}) is None

	assert col.find({"items": {"$contains": 82}}) == doc
	assert col.find({"items": {"$contains": 88}}) == doc
	assert col.find({"items": {"$contains": 90}}) is None

	assert col.find({"items": {"$notcontains": 82}}) is None
	assert col.find({"items": {"$notcontains": 88}}) is None
	assert col.find({"items": {"$notcontains": 90}}) == doc

@arraytest()
def docs_array_query_operators_examples_2(col: PetArray):

	doc = col.insert({"items": [{"product": "abc", "score": 7}, {"product": "xyz", "score": 8}]})

	assert col.find({"items": {"$elemMatch": {"product": "xyz", "score": {"$gte": 8}}}}) == doc
	assert col.find({"items": {"$elemMatch": {"product": "abc", "score": {"$gte": 8}}}}) is None

@arraytest()
def docs_update_operators_examples():

	col = PetArray([{"item": {"amount": 100}}])
	col.update({"$set": {"item.amount": 200, "item.name": "Item 1"}})
	assert col.list() == [{"item": {"amount": 200, "name": "Item 1"}}]

	col = PetArray([{"item": {"amount": 100, "name": "Item 1"}}])
	col.update({"$unset": {"item.amount": True}})
	assert col.list() == [{"item": {"name": "Item 1"}}]
	col.update({"$unset": {"item.name": False}})
	assert col.list() == [{"item": {"name": "Item 1"}}]

	col = PetArray([{"item": {"amount": 100}}])
	col.update({"$map": {"item.amount": lambda val: int(val / 2)}})
	assert col.list() == [{"item": {"amount": 50}}]
	col.update({"$map": {"item": lambda item: {"name": "Item 1", "amount": item["amount"]}}})
	assert col.list() == [{"item": {"name": "Item 1", "amount": 50}}]

	col = PetArray([{"item": {"amount": 100}}])
	col.update({"$inc": {"item.amount": 20}})
	assert col.list() == [{"item": {"amount": 120}}]

@arraytest()
def docs_array_update_operators_examples():

	col = PetArray([{"item": {"skus": [3, 5, 6, 7, 7, 8]}}])
	col.update({"$pull": {"item.skus": {"$gte": 6}}})
	assert col.list() == [{"item": {"skus": [3, 5]}}]
	col.update({"$pull": {"item.skus": 3}})
	assert col.list() == [{"item": {"skus": [5]}}]

	col = PetArray([{"item": {"skus": [3, 5, 6, 7, 7, 8]}}])
	col.update({"$push": {"item.skus": 20}})
	assert col.list() == [{"item": {"skus": [3, 5, 6, 7, 7, 8, 20]}}]

	col = PetArray([{"item": {"skus": [3, 5, 6, 7, 7, 8]}}])
	col.update({"$addToSet": {"item.skus": 20}})
	col.update({"$addToSet": {"item.skus": 6}})
	assert col.list() == [{"item": {"skus": [3, 5, 6, 7, 7, 8, 20]}}]
