import os
import json
import uuid
import shutil
import traceback
from typing import Callable, Any

from petdb import PetDB, PetCollection, PetArray

TEST_COLLECTION_NAME = "testcol"

RED = "\033[91m"
GREEN = "\033[92m"
COLOREND = "\033[0m"

class Tests:
	root: str
	db: PetDB
	col: PetCollection
	array: PetArray
	sets: list[Callable] = []
	tests: list = []

	@classmethod
	def init(cls):
		cls.root = os.path.join(os.getcwd(), "testdb")
		if os.path.exists(cls.root):
			shutil.rmtree(cls.root)
		os.mkdir(cls.root)
		cls.db = PetDB.get(cls.root)

	@classmethod
	def setup(cls, init: list, ignore_id: bool, arraytest: bool):
		if arraytest:
			cls.array = PetArray(init)
			return
		cls.db.drop_collection(TEST_COLLECTION_NAME)
		if not ignore_id:
			for doc in init:
				if isinstance(doc, dict) and "_id" not in doc:
					doc["_id"] = str(uuid.uuid4())
		cls.col = cls.db.collection(TEST_COLLECTION_NAME)
		cls.col.insert_many(init)

	@classmethod
	def run(cls, nums: int | list[int] = None):
		if isinstance(nums, int):
			nums = [nums]
		for testset in cls.sets:
			testset()
		passed = 0
		failed = []
		for i, test in enumerate(cls.tests, 1):
			try:
				if nums is not None and i not in nums:
					continue
				cls.setup(test["init"], test["ignore_id"], test["arraytest"])
				if test["func"].__code__.co_argcount == 1:
					result = test["func"](cls.col if not test["arraytest"] else cls.array)
				else: result = test["func"]()
				if not test["ignore_id"] and not test["arraytest"]:
					cls.remove_ids(result)
					cls.remove_ids(test["expect"])
				if result == test["expect"]:
					passed += 1
				else:
					failed.append(cls.format_failed(i, test, result))
			except Exception as e:
				if isinstance(test["expect"], type) and isinstance(e, test["expect"]):
					passed += 1
				elif isinstance(test["expect"], Exception) and e.args == test["expect"].args:
					passed += 1
				else:
					failed.append(cls.format_exception(i, test))
		shutil.rmtree(cls.root)
		return passed, failed

	@classmethod
	def format_failed(cls, number: int, test: dict, result: Any) -> str:
		return (f"{RED}Test {number} ({test["name"]}){COLOREND}: expected '{test["expect"]}', got '{result}'\n"
				f"File \"{test["func"].__code__.co_filename}\", line {test["func"].__code__.co_firstlineno}")

	@classmethod
	def format_exception(cls, number: int, test: dict) -> str:
		return (f"{RED}Exception raised during Test {number} ({test["name"]}):{COLOREND}\n"
				+ traceback.format_exc().replace("\n\n", "\n"))

	@classmethod
	def remove_ids(cls, entry):
		if isinstance(entry, dict):
			entry.pop("_id", None)
		elif isinstance(entry, list) and len(entry) > 0 and isinstance(entry[0], dict):
			for doc in entry:
				doc.pop("_id", None)

def test(init=None, expect=None, ignore_id: bool = False, init_expectation: bool = True, arraytest: bool = False):
	if init is None and expect and init_expectation:
		if isinstance(expect, dict):
			init = [expect]
		elif isinstance(expect, list) and (all(isinstance(doc, dict) for doc in expect) or arraytest):
			init = expect[:]

	def decorator(func):
		Tests.tests.append({
			"func": func,
			"init": init or [],
			"expect": expect,
			"ignore_id": ignore_id,
			"arraytest": arraytest,
			"name": func.__name__})

	return decorator

def arraytest(init=None, expect=None, ignore_id: bool = False, init_expectation: bool = True):
	return test(init, expect, ignore_id, init_expectation, arraytest=True)

def testset(func):
	Tests.sets.append(func)

def read_collection_file():
	with open(os.path.join(Tests.root, "petstorage", f"{TEST_COLLECTION_NAME}.json")) as f:
		return json.load(f)
