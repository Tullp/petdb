
from petdb import QueryException, PetCollection, NonExistent, PetArray
from .engine import test, arraytest

@test(init=[{}], expect=QueryException("Invalid query: you can't combine operators query with the path objects"))
def operators_queryexception1(col: PetCollection):
	return col.findall({"a": {"b": 5, "$lt": 4}})

@test(expect={"a": 5, "b": 10})
def eq1(col: PetCollection):
	return col.find({"a": {"$eq": 5}})

@test(init=[{"a": 5, "b": 10}], expect=None)
def eq2(col: PetCollection):
	return col.find({"a": {"$eq": 50}})

@test(expect={"a": 5, "b": 10})
def ne1(col: PetCollection):
	return col.find({"a": {"$ne": 1}})

@test(init=[{"a": 5, "b": 10}], expect=None)
def ne2(col: PetCollection):
	return col.find({"a": {"$ne": 5}})

@test(expect=[{"a": 5}, {"b": 10}])
def ne3(col: PetCollection):
	return col.findall({"a": {"$ne": 10}})

@test(init=[{"a": 5, "b": 10}, {"a": 10, "b": 5}, {"a": 1, "b": 0}], expect=[{"a": 1, "b": 0}])
def lt(col: PetCollection):
	return col.findall({"a": {"$lt": 5}})

@test(init=[{"a": 5, "b": 10}, {"a": 10, "b": 5}, {"a": 1, "b": 0}], expect=[{"a": 5, "b": 10}, {"a": 1, "b": 0}])
def lte(col: PetCollection):
	return col.findall({"a": {"$lte": 5}})

@test(init=[{"a": 5, "b": 10}, {"a": 10, "b": 5}, {"a": 1, "b": 0}], expect=[{"a": 10, "b": 5}])
def gt(col: PetCollection):
	return col.findall({"a": {"$gt": 5}})

@test(init=[{"a": 5, "b": 10}, {"a": 10, "b": 5}, {"a": 1, "b": 0}], expect=[{"a": 5, "b": 10}, {"a": 10, "b": 5}])
def gte(col: PetCollection):
	return col.findall({"a": {"$gte": 5}})

@test(init=[{"a": 5, "b": 10}, {"a": 10, "b": 5}, {"a": 1, "b": 0}], expect=[{"a": 5, "b": 10}])
def gt_an_lt(col: PetCollection):
	return col.findall({"a": {"$gt": 3, "$lt": 7}})

@test(init=[{"a": 5, "b": 10}, {"a": 10, "b": 5}, {"a": 1, "b": 0}], expect=[{"a": 5, "b": 10}])
def in1(col: PetCollection):
	return col.findall({"a": {"$in": [4, 5, 6]}})

@test(init=[{"a": 5, "b": 10}, {"a": 10, "b": 5}, {"a": 1, "b": 0}], expect=[])
def in2(col: PetCollection):
	return col.findall({"a": {"$in": [4, 6]}})

@test(init=[{"a": 5, "b": 10}, {"a": 10, "b": 5}, {"a": 1, "b": 0}], expect=[{"a": 10, "b": 5}])
def nin1(col: PetCollection):
	return col.findall({"a": {"$nin": [1, 4, 5, 6]}})

@test(init=[{"a": 5, "b": 10}, {"a": 10, "b": 5}, {"a": 1, "b": 0}], expect=[])
def nin2(col: PetCollection):
	return col.findall({"a": {"$nin": [1, 4, 5, 6, 10]}})

@test(expect=[{"a": 5}])
def exists1(col: PetCollection):
	return col.findall({"a": {"$exists": True}})

@test(init=[{"a": 5}], expect=[])
def exists2(col: PetCollection):
	return col.findall({"b": {"$exists": True}})

@test(init=[{"a": 5}, {"a": 6}, {"b": 10}], expect=[{"a": 5}, {"a": 6}])
def exists3(col: PetCollection):
	return col.findall({"a": {"$exists": True}})

@test(init=[{"a": 5}, {"a": 6}, {"b": 10}], expect=[{"b": 10}])
def exists4(col: PetCollection):
	return col.findall({"a": {"$exists": False}})

@test(expect=[{"a": 5}, {"a": 6}, {"b": 10}])
def exists5(col: PetCollection):
	return col.findall({"a.b": {"$exists": False}})

@test(init=[{"a": 5}, {"a": 6}, {"b": 10}], expect=[])
def exists6(col: PetCollection):
	return col.findall({"a.b": {"$exists": True}})

@test(expect=[{"a": "12345"}, {"a": "67890"}])
def regex1(col: PetCollection):
	return col.findall({"a": {"$regex": "\\d{5}"}})

@test(init=[{"a": "12345"}, {"a": "67890"}], expect=[])
def regex2(col: PetCollection):
	return col.findall({"a": {"$regex": "\\D{5}"}})

@test(init=[{"a": "12345"}, {"a": "678901"}], expect=[{"a": "12345"}])
def regex3(col: PetCollection):
	return col.findall({"a": {"$regex": "^1"}})

@test(init=[{"a": "12345"}, {"a": "678901"}], expect=[{"a": "12345"}])
def regex4(col: PetCollection):
	return col.findall({"a": {"$regex": "5$"}})

@test(init=[{"a": "12345"}, {"a": "abcde"}], expect=[{"a": "12345"}])
def func1(col: PetCollection):
	return col.findall({"a": {"$func": str.isdigit}})

@test(init=[{"a": "12345"}, {"a": "abcde"}, {"a": 12345}], expect=TypeError)
def func2(col: PetCollection):
	return col.findall({"a": {"$func": str.isdigit}})

@test(init=[{"a": "12345"}, {"a": "abcde"}], expect=[{"a": "12345"}])
def func3(col: PetCollection):
	return col.findall({"a": {"$func": str.isdigit}})

@test(init=[{"a": "12345"}, {"a": "abcde"}, {"a": 12345}], expect=[{"a": "12345"}, {"a": "abcde"}])
def type1(col: PetCollection):
	return col.findall({"a": {"$type": str}})

@test(init=[{"a": "12"}, {"a": "ab", "b": 5}, {"a": 123}], expect=[{"a": "12"}, {"a": 123}])
def type2(col: PetCollection):
	return col.findall({"b": {"$type": NonExistent}})

@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=[{"a": 1}, {"a": 3}])
def or1(col: PetCollection):
	return col.findall({"$or": [{"a": 1}, {"a": 3}]})

@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=[{"a": 1}, {"a": 3}])
def or2(col: PetCollection):
	return col.findall({"a": {"$or": [1, 3]}})

@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=[{"a": 1}, {"a": 3}])
def or3(col: PetCollection):
	return col.findall({"a": {"$or": [{"$lt": 2}, {"$eq": 3}]}})

@test(init=[{"a": {"b": 1}}, {"a": {"b": 7}}, {"a": {"b": 10}}], expect=[{"a": {"b": 1}}, {"a": {"b": 10}}])
def or4(col: PetCollection):
	return col.findall({"$or": [{"a.b": 1}, {"a.b": 10}]})

@test(init=[{"a": {"b": 1}}, {"a": {"b": 7}}, {"a": {"b": 10}}], expect=[{"a": {"b": 1}}, {"a": {"b": 10}}])
def or5(col: PetCollection):
	return col.findall({"$or": [{"a.b": {"$lt": 5}}, {"a.b": {"$gte": 10}}]})

@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=[{"a": 2}])
def and1(col: PetCollection):
	return col.findall({"$and": [{"a": {"$gt": 1}}, {"a": {"$lt": 3}}]})

@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}, {"b": 2}], expect=[{"a": 2}])
def and2(col: PetCollection):
	return col.findall({"a": {"$and": [{"$exists": True}, {"$gt": 1}, {"$lt": 3}]}})

@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=[{"a": 1}, {"a": 2}])
def not1(col: PetCollection):
	return col.findall({"$not": {"a": {"$gte": 3}}})

@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=[{"a": 1}, {"a": 2}])
def not2(col: PetCollection):
	return col.findall({"a": {"$not": {"$gte": 3}}})

@test(init=[{"a": 1}, {"a": 2}, {"a": 3}, {"a": 4}], expect=[{"a": 1}, {"a": 2}, {"a": 4}])
def not3(col: PetCollection):
	return col.findall({"a": {"$not": 3}})

@test(init=[{"list": [[], [1, 2, 3, 4]]}, {"list": [[], [1, 2, 3]]}, {"list": [[], [1, 2]]}, {"list": [[], [1]]}],
	expect=[{"list": [[], [1, 2, 3]]}, {"list": [[], [1, 2]]}, {"list": [[], [1]]}])
def size1(col: PetCollection):
	return col.findall({"list.1": {"$size": {"$lt": 4}}})

@test(init=[{"list": [[], [1, 2, 3, 4]]}, {"list": [[], [1, 2, 3]]}, {"list": [[], [1, 2]]}, {"list": [[], [1]]}],
	expect=[{"list": [[], [1, 2, 3]]}])
def size2(col: PetCollection):
	return col.findall({"list.1": {"$size": 3}})

@test(init=[{"list": [1, 2, 3, 4]}, {"list": [1, 2, 3]}, {"list": [1, 2]}, {"list": [1]}],
	expect=[{"list": [1, 2, 3]}, {"list": [1, 2]}, {"list": [1]}])
def size3(col: PetCollection):
	return col.findall({"list": {"$size": {"$lt": 4}}})

@test(init=[{"list": [1, 2, 3, 4]}, {"list": [1, 2, 3]}, {"list": [1, 2]}, {"list": [1]}],
	expect=[{"list": [1, 2, 3]}])
def size4(col: PetCollection):
	return col.findall({"list": {"$size": 3}})

@arraytest(init=[[1, 2, 3, 4], [1, 2, 3], [1, 2], [1]], expect=[[1, 2], [1]])
def size5(array: PetArray):
	return array.findall({"$size": {"$lt": 3}})

@arraytest(init=[[1, 2, 3, 4], [1, 2, 3], [1, 2], [1]], expect=[[1, 2], [1]])
def size6(array: PetArray):
	return array.findall({"$length": {"$lt": 3}})

@test(init=[{"a": [1, 2]}, {"a": [2, 3]}, {"a": [3, 4]}], expect=[{"a": [1, 2]}, {"a": [2, 3]}])
def contains1(col: PetCollection):
	return col.findall({"a": {"$contains": 2}})

@test(init=[{"a": [1, 2]}, {"a": [2, 3]}, {"a": [3, 4]}], expect=[{"a": [3, 4]}])
def notcontains1(col: PetCollection):
	return col.findall({"a": {"$notcontains": 2}})

@test(init=[{"a": [82, 85, 88]}, {"a": [75, 88, 89]}], expect=[{"a": [82, 85, 88]}])
def elem_match1(col: PetCollection):
	return col.findall({"a": {"$elemMatch": {"$gte": 80, "$lt": 85}}})

@arraytest(init=[[82, 85, 88], [75, 88, 89]], expect=[[82, 85, 88]])
def elem_match2(array: PetArray):
	return array.findall({"$elemMatch": {"$gte": 80, "$lt": 85}})

@test(init=[{"a": [{"p": "a", "s": 10}, {"p": "x", "s": 5}]},
		{"a": [{"p": "a", "s": 8}, {"p": "x", "s": 7}]},
		{"a": [{"p": "a", "s": 7}, {"p": "x", "s": 8}]},
		{"a": [{"p": "a", "s": 7}, {"p": "d", "s": 8}]}],
	expect=[{"a": [{"p": "a", "s": 7}, {"p": "x", "s": 8}]}])
def elem_match3(col: PetCollection):
	return col.findall({"a": {"$elemMatch": {"p": "x", "s": {"$gte": 8}}}})
