
from petdb import PetCollection, QueryException, PetArray, PetMutable, NON_EXISTENT
from .engine import testset, test, read_collection_file

@testset
def base():
	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def collection_to_list(col: PetCollection):
		return list(col)

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def iteration1(col: PetCollection):
		return [doc for doc in col]

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[1, 2, 3])
	def iteration2(col: PetCollection):
		return [doc["a"] for doc in col]

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1, "b": 1}, {"a": 2, "b": 2}, {"a": 3, "b": 3}])
	def foreach1(col: PetCollection):
		col.foreach(lambda doc: doc.update({"b": doc["a"]}))
		return col.list()

	@test(expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def foreach_no_dump(col: PetCollection):
		col.foreach(lambda doc: doc.update({"b": doc["a"]}))
		return read_collection_file()

@testset
def insertion():
	@test(expect=[{"a": 5}], init_expectation=False)
	def insert1(col: PetCollection):
		col.insert({"a": 5})
		return list(col)

	@test(expect=[{"a": 1}, {"a": 2}, {"a": 3}], init_expectation=False)
	def insert2(col: PetCollection):
		col.insert({"a": 1})
		col.insert({"a": 2})
		col.insert({"a": 3})
		return list(col)

	@test(expect=True, init_expectation=False)
	def insert3(col: PetCollection):
		doc_org = {"a": 5}
		doc_copy = col.insert(doc_org)
		doc_copy["a"] = 10
		return doc_org["a"] == 5 and col.exists({"a": 10})

	@test(expect=[{"a": 1}, {"a": 2}, {"a": 3}], init_expectation=False)
	def insert_many1(col: PetCollection):
		col.insert_many([{"a": 1}, {"a": 2}, {"a": 3}])
		return list(col)

	@test(expect=True, init_expectation=False)
	def insert_many2(col: PetCollection):
		docs_org = [{"a": 1}, {"a": 2}, {"a": 3}]
		docs_copy = col.insert_many(docs_org)
		docs_copy[1]["a"] = 10
		return docs_org[1]["a"] == 2 and col.exists({"a": 10})

	@test(expect=True)
	def id1(col: PetCollection):
		doc1 = col.insert({"a": 1})
		doc2 = col.insert({"a": 2})
		return ("_id" in doc1 and len(doc1["_id"]) > 8
				and "_id" in doc2 and len(doc2["_id"]) > 8
				and doc1["_id"] != doc2["_id"])

	@test(expect=12345)
	def id2(col: PetCollection):
		col.insert({"a": 5, "_id": 12345})
		return col.find({"a": 5})["_id"]

	@test(expect=True)
	def id3(col: PetCollection):
		doc1, doc2 = col.insert_many([{"a": 1}, {"a": 2}])
		return ("_id" in doc1 and len(doc1["_id"]) > 8
				and "_id" in doc2 and len(doc2["_id"]) > 8
				and doc1["_id"] != doc2["_id"])

	@test(expect=12345)
	def id4(col: PetCollection):
		col.insert_many([{"a": 5, "_id": 12345}])
		return col.find({"a": 5})["_id"]

	@test(expect=[{"a": 5}], init_expectation=False)
	def insert_dump1(col: PetCollection):
		col.insert({"a": 5})
		return read_collection_file()

	@test(expect=[{"a": 5}, {"a": 5}, {"a": 5}], init_expectation=False)
	def insert_dump2(col: PetCollection):
		for i in range(3):
			col.insert({"a": 5})
		return read_collection_file()

	@test(expect=[{"a": 1}, {"a": 2}, {"a": 3}], init_expectation=False)
	def insert_many_dump(col: PetCollection):
		col.insert_many([{"a": 1}, {"a": 2}, {"a": 3}])
		return read_collection_file()

	@test(expect=QueryException("Duplicate id"))
	def insert_queryexception1(col: PetCollection):
		_id = col.insert({"a": 5})["_id"]
		return col.insert({"a": 10, "_id": _id, "b": 5})

	@test(expect=QueryException("Duplicate id"))
	def insert_many_queryexception1(col: PetCollection):
		_id = col.insert_many([{"a": 5}])[0]["_id"]
		return col.insert_many([{"a": 10, "_id": _id, "b": 5}])

	@test(expect=TypeError("Document must be of type dict"))
	def insert_typeexception(col: PetCollection):
		# noinspection PyTypeChecker
		return col.insert(123)

	@test(expect=TypeError("Document must be of type dict"))
	def insert_many_typeexception(col: PetCollection):
		return col.insert_many([{}, 123, {}])

@testset
def updating():
	@test(expect=QueryException("Invalid update query: it should be a dict"))
	def update_queryexception1(col: PetCollection):
		# noinspection PyTypeChecker
		col.update("123")

	@test(expect=QueryException("Invalid query type: query should be dict or list"))
	def update_queryexception2(col: PetCollection):
		# noinspection PyTypeChecker
		col.update({}, "123")

	@test(expect=QueryException("Invalid query: it should be a dict or callable"))
	def update_queryexception3(col: PetCollection):
		# noinspection PyTypeChecker
		col.pick("a").update({}, "123")

	@test(init=[{"a": 3}, {"a": 4}, {"a": 5}], expect=[{"a": 3}, {"a": 4, "b": 1}, {"a": 5}])
	def update_one1(col: PetCollection):
		col.update_one({"$set": {"b": 1}}, {"a": {"$gte": 4}})
		return col.list()

	@test(init=[{"a": 3}, {"a": 4}, {"a": 5}], expect=[{"a": 3}, {"a": 4}, {"a": 5}])
	def update_one2(col: PetCollection):
		col.update_one({"$set": {"b": 1}}, {"a": {"$gte": 6}})
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}])
	def set1(col: PetCollection):
		for doc in col.filter({"a": {"$lt": 3}}):
			col.update_one({"$set": {"b": 10}}, doc["_id"])
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}])
	def set2(col: PetCollection):
		col.update({"$set": {"b": 10}}, col.filter({"a": {"$lt": 3}}).pick("_id").list())
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}])
	def set3(col: PetCollection):
		col.update({"$set": {"b": 10}}, {"a": {"$lt": 3}})
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}])
	def set4(col: PetCollection):
		col.update({"$set": {"b": 10}}, col.filter({"a": {"$lt": 3}}).list())
		return col.list()

	@test(init=[{"a": {"b": [1, [0, 3, {}]]}}], expect=[{"a": {"b": [1, [0, 3, {"c": 5}]]}}])
	def set5(col: PetCollection):
		col.update({"$set": {"a.b.1.2.c": 5}})
		return col.list()

	@test(init=[{"a": {"b": {}}}], expect=[{"a": {"b": {"c": {"d": 5}}}}])
	def set6(col: PetCollection):
		col.update({"$set": {"a.b.c.d": 5}})
		return col.list()

	@test(init=[{"a": {}}], expect=[{"a": {"b": {"c": {"d": 5}}}}])
	def set7(col: PetCollection):
		col.update({"$set": {"a.b.c.d": 5}})
		return col.list()

	@test(init=[{}], expect=[{"a": {"b": {"c": {"d": 5}}}}])
	def set8(col: PetCollection):
		col.update({"$set": {"a.b.c.d": 5}})
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}])
	def set_dump1(col: PetCollection):
		for doc in col.findall({"a": {"$lt": 3}}):
			col.update_one({"$set": {"b": 10}}, doc["_id"])
		return read_collection_file()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}])
	def set_dump2(col: PetCollection):
		col.update({"$set": {"b": 10}}, col.filter({"a": {"$lt": 3}}).pick("_id").list())
		return read_collection_file()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}])
	def set_dump3(col: PetCollection):
		col.update({"$set": {"b": 10}}, {"a": {"$lt": 3}})
		return read_collection_file()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}])
	def set_dump4(col: PetCollection):
		col.update({"$set": {"b": 10}}, col.filter({"a": {"$lt": 3}}).list())
		return read_collection_file()

	@test(init=[{"a": {"b": []}}], expect=QueryException("Invalid set query: path a.b.c.d doesn't exist"))
	def set_queryexception1(col: PetCollection):
		col.update({"$set": {"a.b.c.d": 5}})

	@test(init=[{"a": {"b": []}}], expect=QueryException("Invalid set query: path a.b.c.d doesn't exist"))
	def set_one_queryexception1(col: PetCollection):
		col.update_one({"$set": {"a.b.c.d": 5}})

	@test(init=[{"a": {"b": 0}}], expect=QueryException("Invalid set query: path a.b.c.d doesn't exist"))
	def set_queryexception2(col: PetCollection):
		col.update({"$set": {"a.b.c.d": 5}})

	@test(init=[{"a": {"b": 0}}], expect=QueryException("Invalid set query: path a.b.c.d doesn't exist"))
	def set_one_queryexception2(col: PetCollection):
		col.update_one({"$set": {"a.b.c.d": 5}})

	@test(init=[{"a": {"b": None}}], expect=QueryException("Invalid set query: path a.b.c.d doesn't exist"))
	def set_queryexception3(col: PetCollection):
		col.update({"$set": {"a.b.c.d": 5}})

	@test(init=[{"a": {"b": None}}], expect=QueryException("Invalid set query: path a.b.c.d doesn't exist"))
	def set_one_queryexception3(col: PetCollection):
		col.update_one({"$set": {"a.b.c.d": 5}})

	@test(init=[{"a": {"b": {"c": None}}}], expect=QueryException("Invalid set query: path a.b.c.d doesn't exist"))
	def set_queryexception4(col: PetCollection):
		col.update({"$set": {"a.b.c.d": 5}})

	@test(init=[{"a": {"b": {"c": None}}}], expect=QueryException("Invalid set query: path a.b.c.d doesn't exist"))
	def set_one_queryexception4(col: PetCollection):
		col.update_one({"$set": {"a.b.c.d": 5}})

	@test(init=[{"a": {"b": []}}], expect=QueryException("Invalid set query: list index must contains only digits"))
	def set_queryexception5(col: PetCollection):
		col.update({"$set": {"a.b.c": 5}})

	@test(init=[{"a": {"b": []}}], expect=QueryException("Invalid set query: list index must contains only digits"))
	def set_one_queryexception5(col: PetCollection):
		col.update_one({"$set": {"a.b.c": 5}})

	@test(init=[{"a": {"b": []}}], expect=QueryException("Invalid set query: list index out of range"))
	def set_queryexception6(col: PetCollection):
		col.update({"$set": {"a.b.1": 5}})

	@test(init=[{"a": {"b": []}}], expect=QueryException("Invalid set query: list index out of range"))
	def set_one_queryexception6(col: PetCollection):
		col.update_one({"$set": {"a.b.1": 5}})

	@test(expect=QueryException("Invalid query format: query list should contains only ids or only docs"))
	def set_queryexception7(col: PetCollection):
		col.update({"$set": {"a.b.c.d": 0}}, ["12345", "67890", {"a": 0}])

	@test(expect=QueryException("Invalid query format: query list should contains only ids or only docs"))
	def set_one_queryexception7(col: PetCollection):
		col.update_one({"$set": {"a.b.c.d": 0}}, ["12345", "67890", {"a": 0}])

	@test(init=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def unset1(col: PetCollection):
		col.update({"$unset": {"b": True}})
		return col.list()

	@test(init=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def unset2(col: PetCollection):
		col.update({"$unset": {"b": True}}, {"b": 10})
		return col.list()

	@test(init=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def unset3(col: PetCollection):
		col.update({"$unset": {"b": True}}, {"b": {"$exists": True}})
		return col.list()

	@test(expect=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}])
	def unset4(col: PetCollection):
		col.update({"$unset": {"b": True}}, {"b": {"$exists": False}})
		return col.list()

	@test(init=[{"a": 1, "b": 10, "c": 4}, {"a": 2, "b": 10}, {"a": 3, "c": 4}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def unset5(col: PetCollection):
		col.update({"$unset": {"b": True, "c": True}})
		return col.list()

	@test(init=[{"a": 1, "b": 10, "c": 4}, {"a": 2, "b": 10}, {"a": 3, "c": 4}],
		expect=[{"a": 1, "c": 4}, {"a": 2}, {"a": 3, "c": 4}])
	def unset6(col: PetCollection):
		col.update({"$unset": {"b": True, "c": False}})
		return col.list()

	@test(init=[{"a": 1, "b": 10, "c": 4}, {"a": 2, "b": 10}, {"a": 3, "c": 4}],
		expect=[{"a": 1, "c": 4}, {"a": 2}, {"a": 3, "c": 4}])
	def unset7(col: PetCollection):
		col.update({"$unset": {"b": True}})
		return col.list()

	@test(init=[{"a": {"b": [1, [0, 3, {"c": 5}]]}}], expect=[{"a": {"b": [1, [0, 3, {}]]}}])
	def unset8(col: PetCollection):
		col.update({"$unset": {"a.b.1.2.c": True}})
		return col.list()

	@test(expect=[{"a": {"b": [1, [0, 3, {}]]}}])
	def unset9(col: PetCollection):
		col.update({"$unset": {"a.b.1.2.c": True}})
		return col.list()

	@test(expect=[{"a": {"b": [1, [0, 3]]}}])
	def unset10(col: PetCollection):
		col.update({"$unset": {"a.b.1.2.c": True}})
		return col.list()

	@test(expect=[{"a": {"b": None}}])
	def unset11(col: PetCollection):
		col.update({"$unset": {"a.b.1.2.c": True}})
		return col.list()

	@test(expect=[{}])
	def unset12(col: PetCollection):
		col.update({"$unset": {"a.b.1.2.c": True}})
		return col.list()

	@test(init=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def unset_dump1(col: PetCollection):
		col.update({"$unset": {"b": True}})
		return read_collection_file()

	@test(init=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def unset_dump2(col: PetCollection):
		col.update({"$unset": {"b": True}}, {"b": 10})
		return read_collection_file()

	@test(init=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def unset_dump3(col: PetCollection):
		col.update({"$unset": {"b": True}}, {"b": {"$exists": True}})
		return read_collection_file()

	@test(expect=[{"a": 1, "b": 10}, {"a": 2, "b": 10}, {"a": 3}])
	def unset_dump4(col: PetCollection):
		col.update({"$unset": {"b": True}}, {"b": {"$exists": False}})
		return read_collection_file()

	@test(init=[{"a": 1, "b": 10, "c": 4}, {"a": 2, "b": 10}, {"a": 3, "c": 4}], expect=[{"a": 1}, {"a": 2}, {"a": 3}])
	def unset_dump5(col: PetCollection):
		col.update({"$unset": {"b": True, "c": True}})
		return read_collection_file()

	@test(init=[{"a": 1, "b": 10, "c": 4}, {"a": 2, "b": 10}, {"a": 3, "c": 4}],
		expect=[{"a": 1, "c": 4}, {"a": 2}, {"a": 3, "c": 4}])
	def unset_dump6(col: PetCollection):
		col.update({"$unset": {"b": True, "c": False}})
		return read_collection_file()

	@test(init=[{"a": 1, "b": 10, "c": 4}, {"a": 2, "b": 10}, {"a": 3, "c": 4}],
		expect=[{"a": 1, "c": 4}, {"a": 2}, {"a": 3, "c": 4}])
	def unset_dump7(col: PetCollection):
		col.update({"$unset": {"b": True}})
		return read_collection_file()

	@test(init=[{"a": {"b": [1, [0, 3, {"c": 5}]]}}], expect=[{"a": {"b": [1, [0, 3, {}]]}}])
	def unset_dump8(col: PetCollection):
		col.update({"$unset": {"a.b.1.2.c": True}})
		return read_collection_file()

	@test(expect=[{"a": {"b": [1, [0, 3, {}]]}}])
	def unset_dump9(col: PetCollection):
		col.update({"$unset": {"a.b.1.2.c": True}})
		return read_collection_file()

	@test(expect=[{"a": {"b": [1, [0, 3]]}}])
	def unset_dump10(col: PetCollection):
		col.update({"$unset": {"a.b.1.2.c": True}})
		return read_collection_file()

	@test(expect=[{"a": {"b": None}}])
	def unset_dump11(col: PetCollection):
		col.update({"$unset": {"a.b.1.2.c": True}})
		return read_collection_file()

	@test(expect=[{}])
	def unset_dump12(col: PetCollection):
		col.update({"$unset": {"a.b.1.2.c": True}})
		return read_collection_file()

	@test(init=[{"a": [1, 2]}, {"a": [1, 2, 3, 4]}], expect=[{"a": [1, 2, 123]}, {"a": [1, 2, 3, 4, 123]}])
	def append1(col: PetCollection):
		col.update({"$append": {"a": 123}}, {})
		return col.list()

	@test(expect=[[0, [1, 2, 3, 222]], [0, [4, 5, 6, 222]]], init_expectation=False)
	def append2():
		array = PetArray([[0, [1, 2, 3]], [0, [4, 5, 6]]])
		array.update({"$append": {1: 222}})
		return array.list()

	@test(init=[{"a": [1, {"b": [1, 2]}]}, {"a": [4, {"b": []}]}],
		expect=[{"a": [1, {"b": [1, 2, {"c": 5}]}]}, {"a": [4, {"b": [{"c": 5}]}]}])
	def append3(col: PetCollection):
		col.update({"$append": {"a.1.b": {"c": 5}}})
		return col.list()

	@test(init=[{"a": [1, 2]}, {"a": [1, 2, 3, 4]}], expect=[{"a": [1, 2, 123]}, {"a": [1, 2, 3, 4, 123]}])
	def append_dump1(col: PetCollection):
		col.update({"$append": {"a": 123}}, {})
		return read_collection_file()

	@test(init=[{"a": [1, {"b": [1, 2]}]}, {"a": [4, {"b": []}]}],
		expect=[{"a": [1, {"b": [1, 2, {"c": 5}]}]}, {"a": [4, {"b": [{"c": 5}]}]}])
	def append_dump3(col: PetCollection):
		col.update({"$append": {"a.1.b": {"c": 5}}})
		return read_collection_file()

	@test(init=[{"a": 5}], expect=QueryException("Invalid push query: only lists supports integer keys"))
	def append_queryexception1(col: PetCollection):
		col.update({"$append": {1: "item"}})

	@test(expect=QueryException("Invalid push query: index out of range"), init_expectation=False)
	def append_queryexception2():
		PetArray([[0, [1, 2, 3]], [0, [4, 5, 6]]]).update({"$append": {2: 222}})

	@test(expect=QueryException("Invalid push query: it's impossible to append not to a list"),
		init_expectation=False)
	def append_queryexception3():
		PetArray([[0, {}], [0, {}]]).update({"$append": {1: 222}})

	@test(expect=QueryException("Invalid push query: it's impossible to append not to a list"),
		init_expectation=False)
	def append_queryexception4():
		PetArray([[0, "123"], [0, "123"]]).update({"$append": {1: 222}})

	@test(init=[{"a": {}}], expect=QueryException("Invalid push query: path a.b doesn't exist"))
	def append_queryexception5(col: PetCollection):
		col.update({"$append": {"a.b": 5}})

	@test(init=[{}], expect=QueryException("Invalid push query: path a.b doesn't exist"))
	def append_queryexception6(col: PetCollection):
		col.update({"$append": {"a.b": 5}})

	@test(init=[{"a": {"b": {}}}],
		expect=QueryException(f"Invalid push query: it's impossible to append not to a list"))
	def append_queryexception7(col: PetCollection):
		col.update({"$append": {"a.b": 5}})

	@test(init=[{"a": {"b": 5}}],
		expect=QueryException(f"Invalid push query: it's impossible to append not to a list"))
	def append_queryexception8(col: PetCollection):
		col.update({"$append": {"a.b": 5}})

	@test(init=[{"a": {"b": None}}],
		expect=QueryException(f"Invalid push query: it's impossible to append not to a list"))
	def append_queryexception9(col: PetCollection):
		col.update({"$append": {"a.b": 5}})

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": "2"}, {"a": "3"}, {"a": "4"}])
	def update_map1(col: PetCollection):
		col.update({"$map": {"a": lambda x: str(x + 1)}})
		return col.list()

	@test(init=[{"a": {"b": 1}}, {"a": {"b": 2}}, {"a": {"b": 3}}],
		expect=[{"a": {"b": "2"}}, {"a": {"b": "3"}}, {"a": {"b": "4"}}])
	def update_map2(col: PetCollection):
		col.update({"$map": {"a.b": lambda x: str(x + 1)}})
		return col.list()

	@test(init=[{"a": 1, "b": "3"}, {"a": 2, "b": "2"}, {"a": 3, "b": "1"}],
		expect=[{"a": "2", "b": 9}, {"a": "3", "b": 4}, {"a": "4", "b": 1}])
	def update_map3(col: PetCollection):
		col.update({"$map": {"a": lambda x: str(x + 1), "b": lambda x: int(x) ** 2}})
		return col.list()

	@test(init=[{"a": {"b": 1}}, {"a": {"b": 2}}, {"a": {"b": 3}}],
		expect=[{"a": {"c": "1"}}, {"a": {"c": "2"}}, {"a": {"c": "3"}}])
	def update_map4(col: PetCollection):
		col.update({"$map": {"a": lambda x: {"c": str(x["b"])}}})
		return col.list()

	@test(init=[{"a": 1}, {"a": 2}, {"a": 3}], expect=[{"a": "2"}, {"a": "3"}, {"a": "4"}])
	def update_map_dump1(col: PetCollection):
		col.update({"$map": {"a": lambda x: str(x + 1)}})
		return read_collection_file()

	@test(init=[{"a": {"b": 1}}, {"a": {"b": 2}}, {"a": {"b": 3}}],
		expect=[{"a": {"b": "2"}}, {"a": {"b": "3"}}, {"a": {"b": "4"}}])
	def update_map_dump2(col: PetCollection):
		col.update({"$map": {"a.b": lambda x: str(x + 1)}})
		return read_collection_file()

	@test(init=[{"a": 1, "b": "3"}, {"a": 2, "b": "2"}, {"a": 3, "b": "1"}],
		expect=[{"a": "2", "b": 9}, {"a": "3", "b": 4}, {"a": "4", "b": 1}])
	def update_map_dump3(col: PetCollection):
		col.update({"$map": {"a": lambda x: str(x + 1), "b": lambda x: int(x) ** 2}})
		return read_collection_file()

	@test(init=[{"a": {"b": 1}}, {"a": {"b": 2}}, {"a": {"b": 3}}],
		expect=[{"a": {"c": "1"}}, {"a": {"c": "2"}}, {"a": {"c": "3"}}])
	def update_map_dump4(col: PetCollection):
		col.update({"$map": {"a": lambda x: {"c": str(x["b"])}}})
		return read_collection_file()

	@test(init=[{"list": [1, 2, 3, 4, 5, 6, 7, 8, 9]}], expect=[{"list": [1, 2, 3, 4, 6, 7, 8, 9]}])
	def pull1(col: PetCollection):
		col.update({"$pull": {"list": 5}})
		return col.list()

	@test(init=[{"list": [1, 2, 3, 4, 5, 6, 7, 8, 9]}], expect=[{"list": [6, 7, 9]}])
	def pull2(col: PetCollection):
		col.update({"$pull": {"list": {"$in": [1, 2, 3, 4, 5, 8]}}})
		return col.list()

	@test(init=[{"list": [1, 2, 3, 4, 5, 6, 7, 8, 9]}], expect=[{"list": [1, 3, 5, 7, 9]}])
	def pull3(col: PetCollection):
		col.update({"$pull": {"list": {"$where": lambda x: x % 2 == 0}}})
		return col.list()

	@test(init=[{"a": {"b": [[], [[1, 2, 3, 4, 5]]]}}], expect=[{"a": {"b": [[], [[2, 4]]]}}])
	def pull4(col: PetCollection):
		col.update({"$pull": {"a.b.1.0": {"$where": lambda x: x % 2 == 1}}})
		return col.list()

@testset
def selection():
	@test(expect={"a": 1}, init_expectation=False)
	def get_by_id1(col: PetCollection):
		doc_id = col.insert({"a": 1})["_id"]
		return col.get(doc_id)

	@test(expect=None)
	def get_by_id2(col: PetCollection):
		return col.get("12345")

	@test(expect={"a": 5, "b": 10})
	def find1(col: PetCollection):
		return col.find({"a": 5})

	@test(init=[{"a": 5, "b": 10}], expect=None)
	def find2(col: PetCollection):
		return col.find({"a": 10})

	@test(expect={"a": 5, "b": 10, "c": 20})
	def find3(col: PetCollection):
		return col.find({"a": 5, "c": 20})

	@test(expect={"a": 5, "b": 10, "c": {"d": 20}})
	def find4(col: PetCollection):
		return col.find({"a": 5, "c.d": 20})

	@test(expect={"a": 5, "b": 10, "c": {"d": 20}})
	def find5(col: PetCollection):
		return col.find({"a": 5, "c": {"d": 20}})

	@test(expect={"a": 5, "b": 10, "c": {"d": {"e": 20}}})
	def find6(col: PetCollection):
		return col.find({"a": 5, "c": {"d.e": 20}})

	@test(expect={"a": 5, "b": 10, "c": {"d": {"e": 20}}})
	def find7(col: PetCollection):
		return col.find({"a": 5, "c.d": {"e": 20}})

	@test(expect={"a": 5, "b": 10, "c": {"d": {"e": 20}}})
	def find8(col: PetCollection):
		return col.find({"a": 5, "c": {"d": {"e": 20}}})

	@test(expect=[{"a": 5, "b": 1, "c": 3}, {"a": 6, "b": 1}, {"a": 7, "b": 1}])
	def findall1(col: PetCollection):
		return col.findall({})

	@test(expect=[{"a": 5, "b": 1, "c": 3}, {"a": 6, "b": 1}, {"a": 7, "b": 1}])
	def findall2(col: PetCollection):
		return list(col.findall({}))

	@test(init=[{"a": 5, "c": 3}, {"a": 6, "b": 1}, {"a": 7, "b": 1}], expect=[{"a": 6, "b": 1}, {"a": 7, "b": 1}])
	def findall3(col: PetCollection):
		return col.findall({"b": 1})

	@test(init=[{"a": 5, "b": 2, "c": 3}, {"a": 6, "b": 1}, {"a": 7, "b": 1}],
		expect=[{"a": 6, "b": 1}, {"a": 7, "b": 1}])
	def findall4(col: PetCollection):
		return col.findall({"b": 1})

	@test(init=[{"a": 5, "b": 2, "c": 3}, {"a": 6, "b": 1}, {"a": 7, "b": 1}], expect=[])
	def findall5(col: PetCollection):
		return col.findall({"b": 3})

	@test(init=[{"a": [{}, {"b": 1}]}, {"a": [{}, {"b": 2}]}, {"a": [{}, {"b": 3}]}, {"a": []}],
		expect=[{"a": [{}, {"b": 1}]}, {"a": [{}, {"b": 2}]}])
	def findall6(col: PetCollection):
		return col.filter({"a.1.b": {"$exists": True}}).findall({"a.1.b": {"$lt": 3}})

	@test(init=[{"a": [{}, {"b": 1}]}, {"a": [{}, {"b": 2}]}, {"a": [{}, {"b": 3}]}], expect=[{"a": [{}, {"b": 2}]}])
	def findall7(col: PetCollection):
		return col.filter({"a.1.b": {"$gt": 1}}).findall({"a.1.b": {"$lt": 3}})

	@test(init=[{"a": 5}, {"a": 6}], expect=True)
	def filter1(col: PetCollection):
		return col.filter({"a": 5}).__class__ == PetMutable

	@test(init=[{"a": 5}, {"a": 6}], expect=[{"a": 5}])
	def filter2(col: PetCollection):
		return col.filter({"a": 5}).findall({})

@testset
def deletion():

	@test(init=[{"a": 5}, {"b": 2}, {"b": 3}], expect=[])
	def remove_clear_dump1(col: PetCollection):
		col.remove({})
		return read_collection_file()

	@test(expect=[], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove_clear_dump2(col: PetCollection):
		col.remove({})
		return read_collection_file()

	@test(init=[{"a": 5}, {"b": 2}, {"b": 3}], expect=[])
	def remove_clear1(col: PetCollection):
		col.remove({})
		return col.list()

	@test(expect=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove_clear2(col: PetCollection):
		return col.remove({})

	@test(expect=[], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove_clear3(col: PetCollection):
		col.remove({})
		return col.list()

	@test(expect=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove_clear4(col: PetCollection):
		return col.remove({})

	@test(expect=[{"a": 5}, {"b": 2}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove1(col: PetCollection):
		col.remove({"b": 3})
		return col.list()

	@test(expect=[{"a": 5}, {"b": 2}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove_dump1(col: PetCollection):
		col.remove({"b": 3})
		return read_collection_file()

	@test(expect=[{"b": 2}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove2(col: PetCollection):
		return col.remove({"b": 2})

	@test(expect=[{"a": 5}, {"b": 3}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove3(col: PetCollection):
		col.remove(col.pick("_id")[1])
		return col.list()

	@test(expect=[{"a": 5}, {"b": 3}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove_dump2(col: PetCollection):
		col.remove(col.pick("_id")[1])
		return read_collection_file()

	@test(expect=[{"b": 2}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove4(col: PetCollection):
		return col.remove(col.pick("_id")[1])

	@test(expect=[{"a": 5}, {"b": 3}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove5(col: PetCollection):
		col.remove([col.pick("_id")[1]])
		return col.list()

	@test(expect=[{"a": 5}, {"b": 3}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove_dump3(col: PetCollection):
		col.remove([col.pick("_id")[1]])
		return read_collection_file()

	@test(expect=[{"b": 2}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove6(col: PetCollection):
		return col.remove([col.pick("_id")[1]])

	@test(expect=[{"a": 5}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove7(col: PetCollection):
		col.remove(col[1:])
		return col.list()

	@test(expect=[{"a": 5}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove_dump4(col: PetCollection):
		col.remove(col[1:])
		return read_collection_file()

	@test(expect=[{"b": 2}, {"b": 3}], init=[{"a": 5}, {"b": 2}, {"b": 3}])
	def remove8(col: PetCollection):
		return col.remove(col[1:])

	@test(expect=QueryException("Invalid delete query"))
	def remove_queryexception1(col: PetCollection):
		# noinspection PyTypeChecker
		return col.remove(1)

	@test(expect=QueryException("Invalid delete query: it can only be a list of IDs or a list of docs"))
	def remove_queryexception2(col: PetCollection):
		return col.remove(["abcd", "abcde", {"a": 5}])

@testset
def sorting():
	@test(init=[{"a": 5}, {"a": 4}, {"a": 8}, {"a": 10}, {"a": 2}, {"a": 1}],
		expect=[{"a": 1}, {"a": 2}, {"a": 4}, {"a": 5}, {"a": 8}, {"a": 10}])
	def sort1(col: PetCollection):
		return col.sort("a").list()

	@test(init=[{"a": 5}, {"a": 4}, {"a": 8}, {"b": 0}, {"a": 10}, {"a": 2}, {"a": 1}],
		expect=[{"a": 1}, {"a": 2}, {"a": 4}, {"a": 5}, {"a": 8}, {"a": 10}, {"b": 0}])
	def sort2(col: PetCollection):
		return col.sort("a").list()

	@test(init=[{"a": "5"}, {"a": 4}, {"a": "8"}, {"b": 0}, {"a": 10}, {"a": 2}, {"a": 1}],
		expect=[{'a': 1}, {'a': 2}, {'a': 4}, {'a': 10}])
	def sort3(col: PetCollection):
		return col.filter({"a": {"$type": int}}).sort("a").list()

	@test(init=[{"a": "5"}, {"a": 4}, {"a": "8"}, {"b": 0}, {"a": 10}, {"a": 2}, {"a": 1}],
		expect=[{'a': 10}, {'a': 4}, {'a': 2}, {'a': 1}])
	def sort4(col: PetCollection):
		return col.filter({"a": {"$type": int}}).sort("a", reverse=True).list()

	@test(init=[{"a": 5}, {"a": 4}, {"a": 8}, {"a": 10}, {"a": 2}, {"a": 1}],
		expect=[[1], [2], [4], [5], [8], [10]])
	def sort5(col: PetCollection):
		return col.map(lambda doc: [doc["a"]]).sort(0).list()

	@test(init=[{"a": 5}, {"a": 4}, {"a": 8}, {"a": 10}, {"a": 2}, {"a": 1}],
		expect=[[1], [2], [4], [5], [8], [10]])
	def sort6(col: PetCollection):
		return col.map(lambda doc: [doc["a"]]).sort("0").list()

	@test(init=[{"a": 5}, {"a": 4}, {"a": 8}, {"b": 0}, {"a": 10}, {"a": 2}, {"a": 1}],
		expect=[[1], [2], [4], [5], [8], [10], [None]])
	def sort7(col: PetCollection):
		return col.map(lambda doc: [doc.get("a", None)]).sort(lambda i: (i[0] is None, i[0])).list()

	@test(init=[{"a": 5}, {"a": 4}, {"a": 8}, {"a": 10}, {"a": 2}, {"a": 1}],
		expect=[[[0, 1]], [[0, 2]], [[0, 4]], [[0, 5]], [[0, 8]], [[0, 10]]])
	def sort8(col: PetCollection):
		return col.map(lambda doc: [[0, doc.get("a", None)]]).sort("0.1").list()

	@test(init=[{"a": 5}, {"a": 4}, {"a": 8}, {"b": 0}, {"a": 10}, {"a": 2}, {"a": 1}],
		expect=[1, 2, 4, 5, 8, 10, NON_EXISTENT])
	def sort9(col: PetCollection):
		return col.map(lambda doc: doc.get("a", NON_EXISTENT)).sort().list()

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}],
		expect=[{"a": 4, "b": 4}, {"a": 5, "b": 3}, {"a": 5, "b": 5}])
	def sort10(col: PetCollection):
		return col.sort(("a", "b")).list()

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def sort_col_type(col: PetCollection):
		return col.sort("a").__class__ == PetMutable

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def sort_mut_type(col: PetCollection):
		return col.filter({"a": {"$exists": True}}).sort("a").__class__ == PetMutable

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def sort_arr_type(col: PetCollection):
		return col.pick("a").sort().__class__ == PetArray

@testset
def mapping():
	@test(init=[{"a": 5}, {"a": 10}], expect=[5, 10])
	def map1(col: PetCollection):
		return col.map(lambda doc: doc["a"]).list()

	@test(init=[{"a": 5}, {"b": 1}, {"a": 10}], expect=[25, 0, 100])
	def map2(col: PetCollection):
		return col.map(lambda doc: doc.get("a", 0) ** 2).list()

	@test(init=[{"a": 5}, {"b": 1}, {"a": 10}], expect=[100, 25, 0])
	def map3(col: PetCollection):
		return col.map(lambda doc: doc.get("a", 0) ** 2).sort(reverse=True).list()

	@test(expect=[5, 6, 7, 8, 9])
	def map4(col: PetCollection):
		for i in range(10):
			col.insert({"a": i})
		return col.filter({"a": {"$gte": 5}}).map(lambda doc: doc["a"]).list()

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def map_col_type(col: PetCollection):
		return col.map(lambda x: x).__class__ == PetArray

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def map_mut_type(col: PetCollection):
		return col.filter({"a": {"$exists": True}}).map(lambda x: x).__class__ == PetArray

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def map_arr_type(col: PetCollection):
		return col.pick("a").map(lambda x: x).__class__ == PetArray

@testset
def size():
	@test(expect=100)
	def size1(col: PetCollection):
		for i in range(100):
			col.insert({"a": i})
		return col.size()

	@test(expect=50)
	def size2(col: PetCollection):
		for i in range(100):
			col.insert({"a": i})
		return col.filter({"a": {"$lt": 50}}).size()

	@test(expect=0)
	def size3(col: PetCollection):
		return col.size()

	@test(expect=0)
	def size4(col: PetCollection):
		return col.filter({"a": {"$lt": 50}}).size()

@testset
def pick():

	@test(init=[{"a": 5}, {"a": 6}, {"b": 1}, {"a": 2}], expect=[5, 6, 2])
	def pick1(col: PetCollection):
		return col.pick("a").list()

	@test(init=[{"a": {"b": 5}}, {"a": {"b": 6}}, {"a": {"b": 2}}], expect=[5, 6, 2])
	def pick2(col: PetCollection):
		return col.pick("a.b").list()

	@test(init=[{"a": {"b": 5}}, {"a": {"b": 6}}, {"a": {"b": 2}}], expect=[5, 6, 2])
	def pick3(col: PetCollection):
		return col.pick("a").pick("b").list()

	@test(init=[{"a": [0, {"b": 5}]}, {"a": [0, {"b": 6}]}, {"a": [0, {"b": 2}]}], expect=[5, 6, 2])
	def pick4(col: PetCollection):
		return col.pick("a.1.b").list()

	@test(init=[{"a": [0, {"b": 5}]}, {"a": [0, {"b": 6}]}, {"a": [0, {"b": 2}]}], expect=[5, 6, 2])
	def pick5(col: PetCollection):
		return col.pick("a").pick("1.b").list()

	@test(init=[{"a": [0, {"b": 5}]}, {"a": [0, {"b": 6}]}, {"a": [0, {"b": 2}]}], expect=[5, 6, 2])
	def pick6(col: PetCollection):
		return col.pick("a.1").pick("b").list()

	@test(init=[{"a": [0, {"b": 5}]}, {"a": [0, {"b": 6}]}, {"a": [0, {"b": 2}]}], expect=[5, 6, 2])
	def pick7(col: PetCollection):
		return col.pick("a").pick(1).pick("b").list()

	@test(init=[{"a": [0, {"b": 5}]}, {"a": [0, {"b": 6}]}, {"a": [0, {"b": 2}]}], expect=[5, 6, 2])
	def pick8(col: PetCollection):
		return col.pick("a").pick("1").pick("b").list()

	@test(init=[{"a": 5, "b": 10, "c": 15}, {"a": 20, "b": 25, "c": 30}],
		expect=[{"a": 5, "c": 15}, {"a": 20, "c": 30}])
	def pick9(col: PetCollection):
		return col.pick("a", "c").list()

	@test(init=[{"a": {"b": {"c": 5}}, "z": 0}, {"a": {"b": {"c": 10}}, "z": -1}],
		expect=[{"a": {"b": {"c": 5}}, "z": 0}, {"a": {"b": {"c": 10}}, "z": -1}])
	def pick10(col: PetCollection):
		return col.pick("a", "z").list()

	@test(init=[{"a": {"b": {"c": 5}}, "z": 0}, {"a": {"b": {"c": 10}}, "z": -1}],
		expect=[{"b": {"c": 5}, "z": 0}, {"b": {"c": 10}, "z": -1}])
	def pick11(col: PetCollection):
		return col.pick("a.b", "z").list()

	@test(init=[{"a": {"b": {"c": 5}}, "z": 0}, {"a": {"b": {"c": 10}}, "z": -1}],
		expect=[{"c": 5, "z": 0}, {"c": 10, "z": -1}])
	def pick12(col: PetCollection):
		return col.pick("a.b.c", "z").list()

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def pick_col_type(col: PetCollection):
		return col.pick("a").__class__ == PetArray

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def pick_mut_type(col: PetCollection):
		return col.filter({"a": {"$exists": True}}).pick("a").__class__ == PetArray

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def pick_arr_type(col: PetCollection):
		return col.map(lambda x: x).pick("a").__class__ == PetArray

@testset
def omit():

	@test(init=[{"a": 5}, {"a": 6}, {"b": 1}, {"a": 2}], expect=[{}, {}, {"b": 1}, {}])
	def omit1(col: PetCollection):
		return col.omit("a").list()

	@test(init=[{"a": 5}, {"a": 6}, {"b": 1}, {"a": 2}], expect=[{}, {}, {"b": 1}, {}])
	def omit2(col: PetCollection):
		return col.omit("a", "z").list()

	@test(init=[{"a": 5}, {"a": 6}, {"b": 1}, {"a": 2}], expect=[{}, {}, {}, {}])
	def omit3(col: PetCollection):
		return col.omit("a", "b").list()

	@test(init=[{"a": 5, "b": 10}, {"a": 6, "b": 12}, {"a": 1, "b": 2}], expect=[{"b": 10}, {"b": 12}, {"b": 2}])
	def omit4(col: PetCollection):
		return col.omit("a").list()

	@test(init=[{"a": {"b": 10, "c": 20}}], expect=[{"a": {"c": 20}}])
	def omit5(col: PetCollection):
		return col.omit("a.b").list()

	@test(init=[{"a": {"b": 10, "c": 20}}], expect=[{"a": {}}])
	def omit6(col: PetCollection):
		return col.omit("a.b", "a.c").list()

	@test(init=[{"a": {"b": 10, "c": 20}}], expect=[{}])
	def omit7(col: PetCollection):
		return col.omit("a").list()

	@test(init=[{"a": [0, {"b": {"c": 20, "d": -1}}]}], expect=[{"a": [0, {"b": {"c": 20}}]}])
	def omit8(col: PetCollection):
		return col.omit("a.1.b.d").list()

	@test(init=[{"a": [0, {"b": {"c": 20, "d": -1}}]}], expect=[{"a": [0, {"b": {"c": 20, "d": -1}}]}])
	def omit9(col: PetCollection):
		return col.omit("a.1.b.z").list()

	@test(init=[{"a": [0, {"b": {"c": 20, "d": -1}}]}], expect=[{"a": [0, {"b": {"c": 20, "d": -1}}]}])
	def omit10(col: PetCollection):
		return col.omit("a.2.b.d").list()

	@test(expect=[{"a": 5, "b": 10}, {"a": 50, "b": 100}])
	def omit11(col: PetCollection):
		col.omit("a")
		return col.list()

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def omit_col_type(col: PetCollection):
		return col.omit("a").__class__ == PetArray

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def omit_mut_type(col: PetCollection):
		return col.filter({"a": {"$exists": True}}).omit("a").__class__ == PetArray

	@test(init=[{"a": 5, "b": 5}, {"a": 5, "b": 3}, {"a": 4, "b": 4}], expect=True)
	def omit_arr_type(col: PetCollection):
		return col.map(lambda x: x).omit("a").__class__ == PetArray
