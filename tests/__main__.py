
import sys

from .engine import Tests, GREEN, RED, COLOREND
from .servicedb import run as run_servicedb
# noinspection PyUnresolvedReferences
from .collection import *
# noinspection PyUnresolvedReferences
from .operators import *
# noinspection PyUnresolvedReferences
from .mutable import *
# noinspection PyUnresolvedReferences
from .petarray import *
# noinspection PyUnresolvedReferences
from .docexamples import *

def get_repeated_names() -> str:
	def format_func(func):
		return (f"{func.__name__}: {func.__code__.co_filename}\n"
			f"File \"{func.__code__.co_filename}\", line {func.__code__.co_firstlineno}")
	return PetArray(Tests.tests) \
		.pick("func") \
		.groupby(lambda f: (f.__name__, f.__code__.co_filename)) \
		.filter({"items": {"$length": {"$gt": 1}}}) \
		.pick("items") \
		.map(lambda group: "\n".join(format_func(f) for f in group)) \
		.join("\n\n")

if __name__ == '__main__':
	print("Running tests...\n")
	Tests.init()
	passed, failed = Tests.run([int(arg) for arg in sys.argv[1:]] or None)
	if len(failed) == 0:
		print(f"{GREEN}All tests passed ({passed}){COLOREND}")
	else:
		print("\n".join(failed) + "\n")
		print(f"{RED}Tests failed ({len(failed)}/{len(failed) + passed}){COLOREND}")

	if len(repeated := get_repeated_names()) > 0:
		print(f"\n{RED}REPEATED TEST NAMES:{COLOREND}\n{repeated}")

	run_servicedb()
