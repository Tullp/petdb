
import requests

from petdb.service import PetServiceDB
from .engine import GREEN, COLOREND

class PostMock:

	def __init__(self):
		self.response = None
		self.callbacks = []

	def callback(self, cb):
		self.callbacks.append(cb)

	def data(self, data):
		self.response = data

	def __call__(self, url: str, json: dict = None):
		# print(url, json)
		while self.callbacks:
			self.callbacks.pop(0)(url, json)
		response, self.response = self.response, None
		if callable(response):
			return type("", (), {"json": lambda: response(url, json)})
		return type("MockedResponse", (), {
			"json": lambda: response,
			"headers": {"content-type": "application/json" if response else "text/plain"},
		})

def check(func):
	def dec(*args, **kwargs):
		check.count += 1
		return func(*args, **kwargs)
	return dec

check.count = 0

@check
def equals(actual, expected, message: str = None):
	if actual != expected:
		if message is not None:
			raise AssertionError(f"{message}: {actual} != {expected}")
		raise AssertionError(f"{actual} != {expected}")

@check
def startswith(actual: str, expected: str, message: str = None):
	if not actual.startswith(expected):
		if message is not None:
			raise AssertionError(f"{message}: {actual} not starts with {expected}")
		raise AssertionError(f"{actual} not starts with {expected}")

@check
def not_none(actual, message: str = None):
	if actual is None:
		if message is not None:
			raise AssertionError(f"{message}: actual value is None")
		raise AssertionError("actual value is None")

def run():

	mock = PostMock()
	real_post, requests.post = requests.post, mock

	test_dbname = "testdb"
	test_password = "123456789"
	test_port = 8000

	db = PetServiceDB(test_dbname, test_password, test_port)

	mock.callback(lambda u, d: startswith(u, f"http://127.0.0.1:{test_port}/"))
	db.collections()

	mock.callback(lambda u, d: equals(d.get("password"), test_password))
	db.collections()

	mock.callback(lambda u, d: equals(d.get("dbname"), test_dbname))
	db.collections()

	mock.data(["col1", "col2"])
	equals(db.collections(), ["col1", "col2"])

	col = db.collection("test-col")

	mock.callback(lambda u, d: equals(u, "http://127.0.0.1:8000/update/test-col"))
	mock.callback(lambda u, d: equals(d["update"], {"$set": {"field": "value"}}))
	mock.callback(lambda u, d: equals(d["query"], {"field": "value"}))
	col.update({"$set": {"field": "value"}}, {"field": "value"})

	mock.callback(lambda u, d: equals(u, "http://127.0.0.1:8000/update/test-col"))
	mock.callback(lambda u, d: equals(d["update"], {"$set": {"field": "value"}}))
	mock.callback(lambda u, d: equals(d["query"], None))
	col.update({"$set": {"field": "value"}})

	mock.callback(lambda u, d: equals(u, "http://127.0.0.1:8000/update_one/test-col"))
	mock.callback(lambda u, d: equals(d["update"], {"$set": {"field": "value"}}))
	mock.callback(lambda u, d: equals(d["query"], {"field": "value"}))
	col.update_one({"$set": {"field": "value"}}, {"field": "value"})

	mock.callback(lambda u, d: equals(u, "http://127.0.0.1:8000/insert/test-col"))
	mock.callback(lambda u, d: equals(d["doc"]["field"], "value"))
	mock.callback(lambda u, d: not_none(d["doc"].get("_id")))
	col.insert({"field": "value"})

	mock.callback(lambda u, d: equals(u, "http://127.0.0.1:8000/insert_many/test-col"))
	mock.callback(lambda u, d: equals(d["docs"][0]["field1"], "value1"))
	mock.callback(lambda u, d: not_none(d["docs"][0].get("_id")))
	mock.callback(lambda u, d: equals(d["docs"][1]["field2"], "value2"))
	mock.callback(lambda u, d: not_none(d["docs"][1].get("_id")))
	col.insert_many([{"field1": "value1"}, {"field2": "value2"}])

	mock.callback(lambda u, d: equals(u, "http://127.0.0.1:8000/clear/test-col"))
	col.clear()

	mock.data([1, 2, 3])
	mock.callback(lambda u, d: equals(u, "http://127.0.0.1:8000/mutate/test-col"))
	mock.callback(lambda u, d: equals(d["mutations"], []))
	col.list()

	mock.data([{"field": "value1"}, {"field": "value2"}, {"field": "value3"}])
	mock.callback(lambda u, d: equals(u, "http://127.0.0.1:8000/mutate/test-col"))
	mock.callback(lambda u, d: equals(d["mutations"], [
		{"type": "filter", "args": [{"field": "value"}]},
		{"type": "sort", "args": [None, False]}
	]))
	equals(col.filter({"field": "value"}).sort().join(",", lambda d: d["field"][5]), "1,2,3")

	print(f"{GREEN}All service tests passed ({check.count}){COLOREND}")

if __name__ == "__main__":
	run()
