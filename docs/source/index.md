
# Welcome to PetDB!

![PyPI - Python Version](https://img.shields.io/pypi/pyversions/petdb)
![PyPI - Version](https://img.shields.io/pypi/v/petdb)
![Documentation Status](https://readthedocs.org/projects/petdb/badge/?version=latest)
![PyPI - License](https://img.shields.io/pypi/l/petdb)
![Downloads](https://static.pepy.tech/badge/petdb)

## About

PetDB is a lightweight document-oriented database for pet projects.
It was designed with the convenient and comfortable mongo-like API.
In this package also was implemented the simple array with the same API,
that supports not only dict documents but any built-in python type.

* * *

## Contents

```{toctree}
:maxdepth: 2

getting-started.md
tutorial.md
operators/index.md
examples/index.md
api/index.md
```

