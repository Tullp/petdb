
# Arrays

(push-append)=
## ``$push``, ``$append``

Appends a specified value to an array.

```python
col.insert({"item": {"skus": [3, 5, 6, 7, 7, 8]}})
col.update({"$push": {"item.skus": 20}})
col.list() # [{"item": {"skus": [3, 5, 6, 7, 7, 8, 20]}}]
```

(addtoset)=
## ``$addToSet``

Adds a value to an array unless the value is already present, in which case ``$addToSet`` does nothing to that array.

```python
col.insert({"item": {"skus": [3, 5, 6, 7, 7, 8]}})
col.update({"$addToSet": {"item.skus": 20}})
col.update({"$addToSet": {"item.skus": 6}})
col.list() # [{"item": {"skus": [3, 5, 6, 7, 7, 8, 20]}}]
```

(pull-remove)=
## ``$pull``, ``$remove``

Removes from an existing array all instances of a value or values that match a specified condition.

```python
col.insert({"item": {"skus": [3, 5, 6, 7, 7, 8]}})
col.update({"$pull": {"item.skus": {"$gte": 6}}})
col.list() # [{"item": {"skus": [3, 5]}}]
col.update({"$pull": {"item.skus": 3}})
col.list() # [{"item": {"skus": [5]}}]
```
