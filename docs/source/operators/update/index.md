
# Update Operators

The following modifiers are available for use in update operations,
for example, in {meth}`petdb.PetCollection.update` and {meth}`petdb.PetCollection.update_one`.

````{warning}
Any update operator doesn't support nesting object query, you should only use the dot notation to specify fields.
```python
{"$set": {"info.name": "Item 1"}} # Good
{"$set": {"info": {"name": "Item 1"}}} # Wrong
```
````

## Fields

```{list-table}
:widths: 30 70
:width: 100%

*   - [``$set``](#set)
    - Sets the value of a field in a document.
*   - [``$unset``](#unset)
    - Removes the specified field from a document.
*   - [``$inc``](#inc)
    - Increments the value of the field by the specified amount.
*   - [``$map``](#update-map)
    - Calculates and sets the value of a field depending on its current value.
```

## Arrays

```{list-table}
:widths: 30 70
:width: 100%

*   - [``$push``](#push-append), [``$append``](#push-append)
    - Adds an item to an array.
*   - [``$addToSet``](#addtoset)
    - Adds elements to an array only if they do not already exist in the set.
*   - [``$pull``](#pull-remove), [``$remove``](#pull-remove)
    - Removes all array elements that match a specified query.
```

```{toctree}
:maxdepth: 2
:hidden:

fields.md
arrays.md
```
