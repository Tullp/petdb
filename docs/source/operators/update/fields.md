
# Fields

(set)=
## ``$set``

Replaces the value of a field with the specified value.

```python
col.insert({"item": {"amount": 100}})
col.update({"$set": {"item.amount": 200, "item.name": "Item 1"}})
col.list() # [{"item": {"amount": 200, "name": "Item 1"}}]
```

(unset)=
## ``$unset``

Deletes a particular field.

```python
col.insert({"item": {"amount": 100, "name": "Item 1"}})
col.update({"$unset": {"item.amount": True}})
col.list()  # [{"item": {"name": "Item 1"}}]
col.update({"$unset": {"item.name": False}})
col.list()  # [{"item": {"name": "Item 1"}}]
```

(inc)=
## ``$inc``

Increments a field by a specified value.

```python
col.insert({"item": {"amount": 100}})
col.update({"$inc": {"item.amount": 20}})
col.list() # [{"item": {"amount": 120}}]
```

(update-map)=
## ``$map``

Replaces the value of the specified field with the function return value.
It takes the old value and returns the new one.
If the specified field doesn't exist, it will take {data}`petdb.NON_EXISTENT` object.

```python
col.insert({"item": {"amount": 100}})
col.update({"$map": {"item.amount": lambda val: int(val / 2)}})
col.list() # [{"item": {"amount": 50}}]
col.update({"$map": {"item": lambda item: {"name": "Item 1", "amount": item["amount"]}}})
col.list() # [{"item": {"name": "Item 1", "amount": 50}}]
```
