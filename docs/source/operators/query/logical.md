
# Logical

(and-all)=
## ``$and``, ``$all``

Performs a logical AND operation on an array of one or more queries 
and selects the documents that satisfy all the queries.

```python
col.insert({"item": {"amount": 10}})
col.find({"$and": [{"item.amount": {"$gt": 5}}, {"item.amount": {"$lt": 15}}]}) # found
col.find({"item.amount": {"$and": [{"$gt": 5}, {"$lt": 15}]}}) # found
col.find({"item.amount": {"$and": [{"$gt": 5}, {"$lt": 10}]}}) # None
```

````{note}
PetDB provides an implicit AND operation when specifying a comma separated list of expressions.

```python
col.find({"item.amount": {"$gt": 5, "$lt": 15}}) # found
```
````

(or-any)=
## ``$or``, ``$any``

Performs a logical OR operation on an array of one or more queries 
and selects the documents that satisfy at least one of the queries.

```python
col.insert({"item": {"amount": 10}})
col.find({"$or": [{"item.amount": {"$gt": 9}}, {"item.amount": {"$lt": 5}}]})  # found
col.find({"item.amount": {"$or": [{"$gt": 9}, {"$lt": 5}]}})  # found
col.find({"item.amount": {"$or": [{"$gt": 10}, {"$lt": 5}]}})  # None
```

(not)=
## ``$not``

Performs a logical NOT operation on the specified query and selects the documents that do not match this query.
This includes documents that do not contain the field.

```python
col.insert({"item": {"amount": 10}})
col.find({"$not": {"item.amount": 5}}) # found
col.find({"$not": {"item": {"amount": 5}}}) # found
col.find({"$not": {"item.amount": {"$gt": 10}}}) # found
col.find({"item.amount": {"$not": {"gt": 10}}}) # found
col.find({"item": {"amount": {"$not": {"gt": 10}}}}) # found
col.find({"$not": {"item.amount": 10}})  # None
```
