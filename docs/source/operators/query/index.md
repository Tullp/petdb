
# Query Operators

## Comparison

```{list-table}
:widths: 30 70
:width: 100%

*   - [``$eq``](#eq)
    - Matches values that are equal to a specified value.
*   - [``$ne``](#ne)
    - Matches all values that are not equal to a specified value.
*   - [``$gt``](#gt), [``$>``](#gt)
    - Matches values that are greater than a specified value.
*   - [``$gte``](#gte), [``$>=``](#gte)
    - Matches values that are greater than or equal to a specified value.
*   - [``$lt``](lt), [``$<``](lt)
    - Matches values that are less than a specified value.
*   - [``$lte``](lte), [``$<=``](lte)
    - Matches values that are less than or equal to a specified value.
*   - [``$in``](in)
    - Matches any of the values specified in an array.
*   - [``$nin``](nin)
    - Matches none of the values specified in an array.
```

## Logical

```{list-table}
:widths: 30 70
:width: 100%

*   - [``$and``](#and-all), [``$all``](#and-all)
    - Joins query clauses with a logical AND returns all documents that match the conditions of both clauses.
*   - [``$or``](#or-any), [``$any``](#or-any)
    - Joins query clauses with a logical OR returns all documents that match the conditions of either clause.
*   - [``$not``](#not)
    - Inverts the effect of a query expression and returns documents that do not match the query expression.
```

## Element

```{list-table}
:widths: 30 70
:width: 100%

*   - [``$exists``](#query-exists)
    - Matches documents that have the specified field.
*   - [``$type``](#type-is), [``$is``](#type-is)
    - Selects documents if a field is of the specified type.
```

## Array

```{list-table}
:widths: 30 70
:width: 100%

*   - [``$size``](#size-length), [``$length``](#size-length)
    - Selects documents if the array field is a specified size.
*   - [``$elemMatch``](#elem-match)
    - Selects documents if any element in the array field matches the specified query.
*   - [``$contains``](#query-contains)
    - Matches arrays that contain the specified element.
*   - [``$notcontains``](#notcontains)
    - Matches arrays that don't contain the specified element.
```

## Evaluation

```{list-table}
:widths: 30 70
:width: 100%

*   - [``$regex``](#regex)
    - Selects documents where values match a specified regular expression.
*   - [``$where``](#where-func-f), [``$func``](#where-func-f), [``$f``](#where-func-f)
    - Matches documents that satisfy a Python filter function.
```

```{toctree}
:hidden:

comparison.md
logical.md
element.md
array.md
evaluation.md
```