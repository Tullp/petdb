
# Element

(query-exists)=
## ``$exists``

Matches documents that contain or do not contain a specified field,
including documents where the field value is ``None``.
To match non-existent field you can also use {class}`petdb.NonExistent` type check
or value comparison with its instance: {data}`petdb.NON_EXISTENT`.

```python
col.insert({"item": {"amount": 10}})
col.find({"item.amount": {"$exists": True}})  # found
col.find({"item.amount": {"$exists": False}})  # None
col.find({"item.name": {"$exists": False}})  # found
col.find({"item.name": NON_EXISTENT}) # found
col.find({"item.price": {"$lte": 250}})
# TypeError: '<=' not supported between instances of 'NonExistent' and 'int'
col.find({"item.price": {"$exists": True, "$lte": 250}})  # None
```

(type-is)=
## ``$type``, ``$is``

Selects documents where the value of the field is an instance of the specified type.
Querying by data type is useful when dealing with highly unstructured data where data types are not predictable.
To match non-existent field use {class}`petdb.NonExistent` type check.

```python
col.insert({"item": {"amount": 10}})
col.find({"item.amount": {"$is": int}}) # found
col.find({"item.amount": {"$is": str}}) # None
col.find({"item.name": {"$is": NonExistent}})  # found
```
