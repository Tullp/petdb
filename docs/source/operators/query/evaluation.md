
# Evaluation

(regex)=
## ``$regex``

Provides regular expression capabilities for pattern matching strings in queries.
The ``$regex`` operator doesn't support any options and uses the default python's ``re.search`` function for matching.

```python
col.insert_many([
	{"sku": "abc123"},
	{"sku": "abc789"},
	{"sku": "xyz456"},
	{"sku": "xyz789"},
	{"sku": "Abc789"},
])
col.filter({"sku": {"$regex": "789$"}}).pick("sku") # abc789, xyz789, Abc789
col.filter({"sku": {"$regex": "^xyz"}}).pick("sku") # xyz456, xyz789
col.filter({"sku": {"$regex": "[aA]bc"}}).pick("sku") # abc123, abc789, Abc789
```

(where-func-f)=
## ``$where``, ``$func``, ``$f``

Takes a python function, which retrieves the value of the specified field and returns ``True`` or ``False``.
Selects the documents where the return value of the specified function is ``True``.

```python
col.insert({"item": {"amount": 10}})
col.find({"$where": lambda doc: "item" in doc and "amount" in doc["item"]}) # found
col.find({"item": {"amount": {"$where": lambda x: x % 2 == 0}}}) # found
col.find({"item": {"$where": lambda subdoc: subdoc["amount"] > 5}}) # found
col.find({"item": {"$where": lambda subdoc: subdoc["amount"] > 15}}) # None
```
