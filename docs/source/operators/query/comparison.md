
# Comparison

(eq)=
## ``$eq``

Matches documents where the value of a field equals the specified value.
It is equivalent to using the form ``{ field: <value> }``.

```python
col.insert({"item": {"amount": 10}})
col.find({"item.amount": 10}) # found
col.find({"item.amount": {"$eq": 10}}) # found
col.find({"item.amount": {"$eq": 0}}) # None
```

(ne)=
## ``$ne``

Selects the documents where the value of the specified field is not equal to the specified value.
This includes documents that do not contain the specified field.

```python
col.insert({"item": {"amount": 10}})
col.find({"item.amount": {"$ne": 0}}) # found
col.find({"item.amount": {"$ne": 10}}) # None
```

(gt)=
## ``$gt``, ``$>``

Selects the documents where the value of the specified field is greater than (i.e. >) the specified value.

```python
col.insert({"item": {"amount": 10}})
col.find({"item.amount": {"$gt": 9}})  # found
col.find({"item.amount": {"$gt": 10}})  # None
```

(gte)=
## ``$gte``, ``$>=``

Selects the documents where the value of the specified field is greater than or equal to (i.e. >=) the specified value.

```python
col.insert({"item": {"amount": 10}})
col.find({"item.amount": {"$gte": 10}})  # found
col.find({"item.amount": {"$gte": 11}})  # None
```

(lt)=
## ``$lt``, ``$<``

Selects the documents where the value of the specified field is less than (i.e. <) the specified value.

```python
col.insert({"item": {"amount": 10}})
col.find({"item.amount": {"$lt": 11}})  # found
col.find({"item.amount": {"$lt": 10}})  # None
```

(lte)=
## ``$lte``, ``$<=``

Selects the documents where the value of the specified field is less than or equal to (i.e. <=) the specified value.

```python
col.insert({"item": {"amount": 10}})
col.find({"item.amount": {"$lte": 10}})  # found
col.find({"item.amount": {"$lt": 9}})  # None
```

(in)=
## ``$in``

Selects the documents where the value of a field equals any value in the specified array.

```python
col.insert({"item": {"amount": 10}})
col.find({"item.amount": {"$in": [5, 10, 15]}})  # found
col.find({"item.amount": {"$in": [5, 15]}})  # None
```

(nin)=
## ``$nin``

Selects the documents where the value of a field is not in the specified array or the specified field does not exist.

```python
col.insert({"item": {"amount": 10}})
col.find({"item.amount": {"$nin": [5, 15]}})  # found
col.find({"item.amount": {"$nin": [5, 10, 15]}})  # None
```
