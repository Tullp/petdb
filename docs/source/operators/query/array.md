
# Array

(size-length)=
## ``$size``, ``$length``

Matches any array whose number of elements matches the specified value or query.

```python
col.insert({"items": [82, 85, 88]})
col.find({"items": {"$size": 3}}) # found
col.find({"items": {"$size": {"$lt": 10}}}) # found
col.find({"items": {"$size": {"$or": [{"$gt": 10}, {"$lte": 3}]}}}) # found
```

(query-contains)=
## ``$contains``

Selects the documents where the value of a field is an array that contains the specified element.

```python
col.insert({"items": [82, 85, 88]})
col.find({"items": {"$contains": 82}}) # found
col.find({"items": {"$contains": 88}}) # found
col.find({"items": {"$contains": 90}}) # None
```

(notcontains)=
## ``$notcontains``

Selects the documents where the value of a field is an array that doesn't contain the specified element.

```python
col.insert({"items": [82, 85, 88]})
col.find({"items": {"$notcontains": 82}})  # None
col.find({"items": {"$notcontains": 88}})  # None
col.find({"items": {"$notcontains": 90}})  # found
```

(elem-match)=
## ``$elemMatch``

Matches documents that contain an array field with at least one element that matches all the specified query criteria.

```python
col.insert({"items": [82, 85, 88]})
col.find({"items": {"$elemMatch": {"$gte": 80, "$lt": 85}}}) # found
col.find({"items": {"$elemMatch": {"$gte": 83, "$lt": 85}}}) # None
```

```python
col.insert({"items": [{"product": "abc", "score": 7}, {"product": "xyz", "score": 8}]})
col.find({"items": {"$elemMatch": {"product": "xyz", "score": {"$gte": 8}}}}) # found
col.find({"items": {"$elemMatch": {"product": "abc", "score": {"$gte": 8}}}}) # None
```
