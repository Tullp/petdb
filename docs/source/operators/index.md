
# Operators

{doc}`./query/index`
: Query operators provide ways to locate data within the database.

{doc}`./update/index`
: Update operators are operators that enable you to modify the data in your database or add additional data.

```{toctree}
:maxdepth: 2
:hidden:

query/index.md
update/index.md
```
