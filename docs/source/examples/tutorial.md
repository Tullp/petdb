
# From tutorial

```pycon
>>> import pprint
>>> import datetime
>>> from petdb import PetDB
>>> db = PetDB.get()
>>> post = {
...     "author": "Mike",
...     "text": "My first blog post!",
...     "tags": ["petdb", "python"],
...     "date": datetime.now().timestamp(),
... }
>>> posts = db.posts
>>> inserted = posts.insert(post)
>>> inserted
{'author': 'Mike', 'text': 'My first blog post!', 'tags': ['petdb', 'python'],
'date': 1707761881.616323, '_id': '09727c8d-c188-4b7e-993d-e4107034315c'}
>>> pprint.pprint(posts.find({}))
{'_id': '09727c8d-c188-4b7e-993d-e4107034315c',
 'author': 'Mike',
 'date': 1707761881.616323,
 'tags': ['petdb', 'python'],
 'text': 'My first blog post!'}
>>> pprint.pprint(posts.find({"author": "Mike"}))
{'_id': '09727c8d-c188-4b7e-993d-e4107034315c',
 'author': 'Mike',
 'date': 1707761881.616323,
 'tags': ['petdb', 'python'],
 'text': 'My first blog post!'}
>>> posts.find({"author": "Eliot"})
>>> pprint.pprint(posts.find({"_id": "09727c8d-c188-4b7e-993d-e4107034315c"}))
{'_id': '09727c8d-c188-4b7e-993d-e4107034315c',
 'author': 'Mike',
 'date': 1707761881.616323,
 'tags': ['petdb', 'python'],
 'text': 'My first blog post!'}
>>> pprint.pprint(posts.get("09727c8d-c188-4b7e-993d-e4107034315c"))
{'_id': '09727c8d-c188-4b7e-993d-e4107034315c',
 'author': 'Mike',
 'date': 1707761881.616323,
 'tags': ['petdb', 'python'],
 'text': 'My first blog post!'}
>>> new_posts = [
...     {
...         "author": "Mike",
...         "text": "Another post!",
...         "tags": ["multiple", "insert"],
...         "date": datetime.datetime(2020, 11, 12, 11, 14).timestamp(),
...     },
...     {
...         "author": "Eliot",
...         "title": "PetDB is fun",
...         "text": "and pretty easy too!",
...         "date": datetime.datetime(2022, 5, 10, 14, 45).timestamp(),
...     },
... ]
>>> result = posts.insert_many(new_posts)
>>> result
[{'author': 'Mike', 'text': 'Another post!', 'tags': ['multiple', 'insert'], 'date': 1605172440.0, '_id': '6d60c18a-e647-4431-b6f2-7c60cfbba4b2'},
{'author': 'Eliot', 'title': 'PetDB is fun', 'text': 'and pretty easy too!', 'date': 1652183100.0, '_id': 'f294d7c0-795b-4fbe-9436-b8800ec5e845'}]
>>> for post in posts.findall({}):
...     pprint.pprint(post)
...
{'_id': '09727c8d-c188-4b7e-993d-e4107034315c',
 'author': 'Mike',
 'date': 1707761881.616323,
 'tags': ['petdb', 'python'],
 'text': 'My first blog post!'}
{'_id': '6d60c18a-e647-4431-b6f2-7c60cfbba4b2',
 'author': 'Mike',
 'date': 1605172440.0,
 'tags': ['multiple', 'insert'],
 'text': 'Another post!'}
{'_id': 'f294d7c0-795b-4fbe-9436-b8800ec5e845',
 'author': 'Eliot',
 'date': 1652183100.0,
 'text': 'and pretty easy too!',
 'title': 'PetDB is fun'}
>>> for post in posts.findall({"author": "Mike"}):
...     pprint.pprint(post)
...
{'_id': '09727c8d-c188-4b7e-993d-e4107034315c',
 'author': 'Mike',
 'date': 1707761881.616323,
 'tags': ['petdb', 'python'],
 'text': 'My first blog post!'}
{'_id': '6d60c18a-e647-4431-b6f2-7c60cfbba4b2',
 'author': 'Mike',
 'date': 1605172440.0,
 'tags': ['multiple', 'insert'],
 'text': 'Another post!'}
>>> posts.size()
3
>>> posts.size({"author": "Mike"})
2
>>> d = datetime.datetime(2023, 11, 12, 12).timestamp()
>>> for post in posts.filter({"date": {"$lt": d}}).sort("author"):
...     pprint.pprint(post)
... 
{'_id': 'f294d7c0-795b-4fbe-9436-b8800ec5e845',
 'author': 'Eliot',
 'date': 1652183100.0,
 'text': 'and pretty easy too!',
 'title': 'PetDB is fun'}
{'_id': '6d60c18a-e647-4431-b6f2-7c60cfbba4b2',
 'author': 'Mike',
 'date': 1605172440.0,
 'tags': ['multiple', 'insert'],
 'text': 'Another post!'}
```
