
# Examples

Here are a few PetDB usage examples.
Check the [git repo](https://gitlab.com/Tullp/petdb/-/tree/master/examples) for more.

```{toctree}
:maxdepth: 2

demo.md
tutorial.md
```
