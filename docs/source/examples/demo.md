
# Demo

Here is a small demonstration how you can use PetDB:

```pycon
>>> from petdb import PetDB
>>> db = PetDB.get()
>>> users = db["users"]
>>> users.insert_many([
...     {"name": "John", "age": 28, "subscriptions": ["tv", "cloud"]},
...     {"name": "Michael", "age": 32, "subscriptions": ["tv"]},
...     {"name": "Sam", "age": 18, "subscriptions": ["music", "cloud"]},
...     {"name": "Alex", "age": 24, "subscriptions": ["tv", "music"]},
...     {"name": "Bob", "age": 18, "subscriptions": ["music"]},
... ])
[{"name": "John", "age": 28, "subscriptions": ["tv", "cloud"], "_id": "..."}, ...]
>>> users.update(
...     {"$append": {"subscriptions": "games"}},
...     {"age": {"$lt": 25}, "subscriptions": {"$contains": "music"}}
... )
>>> selection = users.filter({"age": 18}).sort("name")
>>> print(selection.pick("name").list())
["Bob", "Sam"]
>>> selection.remove()
[{"name": "Bob", "age": 18, "subscriptions": ["music", "games"], "_id": "..."},
{"name": "Sam", "age": 18, "subscriptions": ["music", "cloud", "games"], "_id": "..."}]
>>> for user in users:
...     print(user)
...
{'name': 'John', 'age': 28, 'subscriptions': ['tv', 'cloud'], '_id': '...'}
{'name': 'Michael', 'age': 32, 'subscriptions': ['tv'], '_id': '...'}
{'name': 'Alex', 'age': 24, 'subscriptions': ['tv', 'music', 'games'], '_id': '...'}
```
