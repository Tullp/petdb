
# PetDB

```{eval-rst}
.. autoclass:: petdb.PetDB
   :members:
   :special-members: __getattr__, __getitem__
```
