
# Getting Started

## Installing with pip

We recommend using [pip](http://pypi.python.org/pypi/pip) to install petdb on all platforms:

```bash
$ python -m pip install petdb
```

To get a specific version of petdb::

```bash
$ python -m pip install petdb==0.4.8
```

To upgrade using pip:

```bash
$ python -m pip install --upgrade petdb
```

## Installing from source

You can also download the project source and install it:

```bash
$ git clone https://gitlab.com/tullp/petdb.git
$ cd petdb/
$ pip install .
```
